from django.contrib import admin
from django.urls import include, path
from django.conf.urls import url, include
from rest_framework import routers
from api import views
from rest_framework_jwt.views import obtain_jwt_token, refresh_jwt_token
from django.conf import settings
from django.conf.urls.static import static
from api.views import CustomUserUpdateView, CustomUserPartialUpdateView

router = routers.DefaultRouter()
router.register(r'users', views.CustomUserViewSet)
router.register(r'news', views.NewsViewSet)

# Wire up our API using automatic URL routing.
# Additionally, we include login URLs for the browsable API.

urlpatterns = [
    path('admin/', admin.site.urls),
    path('', include(router.urls)),
    path('api-auth/', include('rest_framework.urls', namespace='rest_framework')),
    path(r'api-token-auth/', obtain_jwt_token),
    path(r'api-token-refresh/', refresh_jwt_token),

    url(r'^file/', include('file_app.urls')),
    url(r'^users/update/(?P<pk>\d+)/$', CustomUserUpdateView.as_view(), name='user_update'),
    url(r'^users/update-partial/(?P<pk>\d+)/$', CustomUserPartialUpdateView.as_view(), name='user_partial_update'),
]
if settings.DEBUG:
  urlpatterns += static(settings.MEDIA_URL, document_root=settings.MEDIA_ROOT)