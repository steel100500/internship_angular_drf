from rest_framework.mixins import UpdateModelMixin
from rest_framework.viewsets import ModelViewSet

from rest_framework import serializers
from api.models import News, CustomUser
from django.contrib.auth.models import User as AuthUser

class CustomUserSerializer(serializers.HyperlinkedModelSerializer):
    class Meta:
        model = CustomUser
        fields = (
            'id',
            'username',
            'password',
            'email',
            'first_name',
            'last_name',
            'is_active',
            'avatar'
        )
        extra_kwargs = {'password' : {'write_only': True, 'required': True}}

    # CREATE TOKEN
    def create(self, validated_data):
        user = super().create(validated_data)
        if 'password' in validated_data:
            user.set_password(validated_data['password'])
            user.save()
        return user

class NewsSerializer(serializers.ModelSerializer):
    class Meta:
        model = News
        fields = [
            'id',
            'title',
            'desc',
            'tags',
            'author',
            'image'
        ]
