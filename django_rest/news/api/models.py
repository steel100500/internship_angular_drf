from django.db import models
from django.contrib.postgres.fields import ArrayField

from django.contrib.auth.models import AbstractUser


class CustomUser(AbstractUser):
    avatar = models.CharField(max_length=256, null=True)


class News(models.Model):
    title = models.CharField(max_length=32)
    desc = models.TextField()
    tags = ArrayField(models.CharField(max_length=20), blank=True, null=True)
    author = models.ForeignKey('CustomUser', on_delete=models.CASCADE, null=True)
    image = models.CharField(max_length=256, null=True)

    class Meta:
            ordering = ('-id',)
