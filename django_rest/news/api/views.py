# from django.contrib.auth.models import User, News
from rest_framework import viewsets, status, generics
from .serializers import CustomUserSerializer, NewsSerializer

from rest_framework.generics import GenericAPIView
from rest_framework.mixins import UpdateModelMixin
from rest_framework.viewsets import ModelViewSet

from .models import News, CustomUser
from django.shortcuts import render
from rest_framework.response import Response
from . import serializers
from rest_framework.permissions import IsAuthenticated


class CustomUserViewSet(viewsets.ModelViewSet):
    """
    API endpoint that allows users to be viewed or edited.
    """
    queryset = CustomUser.objects.all()
    serializer_class = CustomUserSerializer

class CustomUserUpdateView(GenericAPIView, UpdateModelMixin):
    '''
    CustomUser update API, need to submit both 'name' and 'author_name' fields
    At the same time, or django will prevent to do update for field missing
    '''
    queryset = CustomUser.objects.all()
    serializer_class = CustomUserSerializer

    def put(self, request, *args, **kwargs):
        return self.update(request, *args, **kwargs)

class CustomUserPartialUpdateView(GenericAPIView, UpdateModelMixin):
    '''
    You just need to provide the field which is to be modified.
    '''
    queryset = CustomUser.objects.all()
    serializer_class = CustomUserSerializer

    def put(self, request, *args, **kwargs):
        return self.partial_update(request, *args, **kwargs)


class NewsViewSet(viewsets.ModelViewSet):
    queryset = News.objects.all()
    serializer_class = NewsSerializer
