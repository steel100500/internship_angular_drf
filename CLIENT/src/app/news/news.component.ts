import { Component, OnInit, Inject } from '@angular/core';
import { NewsService } from './news.service';
import { first } from 'rxjs/operators';
import { Router } from '@angular/router';
import { FilterService } from '../filter.service';

export interface Filter {
  value: string;
  viewValue: string;
}


export interface DialogData {
  title: string;
  desc: string;
}

@Component({
  selector: 'app-news',
  template:`<p>TEST</p>`,
  templateUrl: './news.component.html',
  styleUrls: [
    './news.component.css',
    '../app.component.css'
  ],
  providers: [NewsService]
})

export class NewsComponent implements OnInit {
  news: any = [];
  users: any = [];

  page: number = 1;

  // selectedUser: User;
  public token: string;
  public usernameToken: string;
  userName: any = [];
  userIDToken: number;
  // сообщения об ошибках авторизации
  public errors: any = [];
  message: any = [];

  filters: Filter[] = [
    {value: 'all-0', viewValue: 'All'},
    {value: 'tags-1', viewValue: 'Tags'},
    {value: 'authors-2', viewValue: 'Authors'}
  ];

  constructor(
    private newsService: NewsService,
    private router: Router,
    private filterData: FilterService) {
  }
  receiveNews($event) {
    console.log('RECEIVE!!!!!!!!',$event);
    this.news = $event;
  }
  newMessage() {
    // console.log('Changing on news...', this.filterData.currentMessage);
    // if (this.message.lenght>0){
    //   this.news = this.message;
    // }

    // console.log('news:', this.news);
    // // this.filterData.changeMessage("Hello WORLD!!!!!");
    // console.log('message',this.message);
  }

  ngOnInit() {
    this.token = localStorage.getItem('authToken');
    if (this.token == null) {
      this.router.navigate(['/auth/login']);
    }
    const token_parts = this.token.split(/\./);
    const token_decoded = JSON.parse(window.atob(token_parts[1]));
    this.userIDToken = token_decoded.user_id;
    this.getAllUsers();
    this.getAllNews();
    this.filterData.currentMessage.subscribe(message => 
      this.message=message);
    // console.log('MESSAGE FROM FILTER:',this.message);

  }
  //USERS
  getAllUsers(){
    this.newsService.getAllUsers()
    .pipe(first()).subscribe(response => {
      this.users = response['results'];
      // console.log(this.users);
    });
  }
  getAllNews(){
    //NEWS
    this.newsService.getAllNews()
    .pipe(first()).subscribe(response => {
      this.news = response['results'];
      this.news = this.news.map(newsitem => {
        let users = this.users.map(useritem => {
          return useritem
        });
        let userNames = users.find(item => {
          if (item.id === newsitem.author) {
            newsitem.author_id = item.id;
            newsitem.author = item.username;
            // console.log(newsitem);
            return item.username
          }
          return false
        });
        return newsitem
      });
    });
  }
  getUserPage(UserID) {
    if (UserID == undefined) {
      this.router.navigate(['profile/:' + this.userIDToken + '/'])
    }
    else {
      this.router.navigate(['profile/:' + UserID + '/'])
    }
  }
}

