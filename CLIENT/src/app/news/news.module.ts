import { NgModule } from '@angular/core';
import { NewsComponent } from './news.component';
import { CommonModule } from '@angular/common';
import { NewsMaterialModule } from './news-material.module';
import { FormsModule } from '@angular/forms';
import { RouterModule } from '@angular/router';
import { FilterPipe} from '../filter.pipe';
import { AuthModule } from '../auth/auth.module';
import { TagsModule } from '../tags/tags.module';
import { NgxPaginationModule } from 'ngx-pagination';

@NgModule({
    imports: [
        CommonModule,
        NewsMaterialModule,
        FormsModule,
        RouterModule,
        NgxPaginationModule,
        AuthModule,
        TagsModule,
        ],
    declarations: [NewsComponent, FilterPipe],
    exports: [FilterPipe, ],
    entryComponents: [NewsComponent ],
    providers: []
})
export class NewsModule { }