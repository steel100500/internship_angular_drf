import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})

export class NewsService {

  baseurl = 'http://localhost:8002';

  constructor(private http: HttpClient) {
   }

  getAllNews() {
    return this.http.get(this.baseurl + '/news/', {
      headers: {
      Authorization: localStorage.getItem('authToken'),
      'Content-Type': 'application/json'
    }
  });
  }

  getAllUsers() {
    return this.http.get(this.baseurl + '/users/', {
      headers: {
      Authorization: localStorage.getItem('authToken'),
      'Content-Type': 'application/json'
    }
  });
  }
}
