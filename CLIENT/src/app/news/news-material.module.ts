
import { NgModule } from '@angular/core';
import {
  MatButtonModule, MatSelectModule,
  MatChipsModule, MatCardModule,
  MatIconModule, MatAutocompleteModule,
  MatInputModule, MatToolbarModule,
  MatCheckboxModule, MatDialogModule, MatFormFieldModule, MatDividerModule } from "@angular/material";

@NgModule({
  declarations: [],
  imports: [
    MatButtonModule,
    MatChipsModule,
    MatCardModule,
    MatIconModule,
    MatInputModule,
    MatToolbarModule,
    MatSelectModule,
    MatAutocompleteModule,
    MatInputModule,
    MatCheckboxModule,
    MatDialogModule,
    MatFormFieldModule,
    MatDividerModule
  ],
  exports: [
    MatButtonModule,
    MatChipsModule,
    MatCardModule,
    MatIconModule,
    MatInputModule,
    MatToolbarModule,
    MatSelectModule,
    MatAutocompleteModule,
    MatInputModule,
    MatCheckboxModule,
    MatDialogModule,
    MatFormFieldModule,
    MatDividerModule
  ],
})
export class NewsMaterialModule { }
