import { Component, OnInit } from '@angular/core';
import { UsersService } from './users.service';
import { first } from 'rxjs/operators';
import { User } from '../user';
import { Router } from '@angular/router';

@Component({
  selector: 'app-users',
  templateUrl: './users.component.html',
  styleUrls: [
    './users.component.css',
    '../app.component.css'
  ],
  providers: [UsersService]
})
export class UsersComponent implements OnInit {
  page: number = 1;
  users: any = [];
  pageOfItems: Array<any>;
  selectedUser: User;
  public token: string;
  public usernameToken: string;
  userName: any = [];
  userIDToken: number;
  // сообщения об ошибках авторизации
  public errors: any = [];

  constructor(private usersService: UsersService,
    private router: Router) {
  }
  ngOnInit() {
    this.getAllUsers();
    this.token = localStorage.getItem('authToken');
    if (this.token == null) {
      this.router.navigate(['/auth/login']);
    }
    const token_parts = this.token.split(/\./);
    const token_decoded = JSON.parse(window.atob(token_parts[1]));
    this.userIDToken = token_decoded.user_id;

  }

  getAllUsers() {
    this.usersService.getAllUsers()
    .pipe(first()).subscribe(response => {
      this.users = response['results'];
    });
  }

  getUserPage(UserID) {
    if (UserID == undefined) {
      this.router.navigate(['profile/:' + this.userIDToken + '/'])
    }
    else {
      this.router.navigate(['profile/:' + UserID + '/'])
    }
  }

  onSelect(user: User): void {
    this.selectedUser = user;
  }
}
