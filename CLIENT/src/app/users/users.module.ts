import { NgModule } from '@angular/core';
import { UsersComponent } from './users.component';
import { CommonModule } from '@angular/common';
import { UsersMaterialModule } from './users-material.module';
import { FormsModule } from '@angular/forms';
import { RouterModule } from '@angular/router';
import { AuthModule } from '../auth/auth.module';
import { NgxPaginationModule } from 'ngx-pagination';

@NgModule({
    imports: [CommonModule,
        UsersMaterialModule,
        FormsModule,
        RouterModule,
        AuthModule,
        NgxPaginationModule
    ],
    declarations: [UsersComponent],
    entryComponents: [UsersComponent]
})
export class UsersModule { }