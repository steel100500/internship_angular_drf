import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})

export class UsersService {

  baseurl = 'http://localhost:8002';
  httpHeaders = new HttpHeaders({'Content-Type': 'application/json'});

  constructor(private http: HttpClient) {
   }

  getAllUsers() {
    return this.http.get(this.baseurl + '/users/');
  }
}
