import { NgModule } from '@angular/core';
import { TagsComponent } from './tags.component';
import { CommonModule } from '@angular/common';
import { TagsMaterialModule } from './tags-material.module';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { RouterModule } from '@angular/router';

@NgModule({
    imports: [CommonModule, TagsMaterialModule, FormsModule, RouterModule, ReactiveFormsModule],
    declarations: [TagsComponent],
    exports: [ TagsComponent ],
    entryComponents: [TagsComponent]
})
export class TagsModule { }