import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { authRoutes } from './auth/auth.routing';
import { NewsComponent } from './news/news.component';
import { UsersComponent } from './users/users.component';
import { ProfileComponent } from './profile/profile.component';

const routes: Routes = [
  ...authRoutes,
  { path: '' ,
  children: [
    {path: 'news', component: NewsComponent},
    {path: 'users', component: UsersComponent},
    {path: 'profile/:id', component: ProfileComponent},
  ]

  },
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
