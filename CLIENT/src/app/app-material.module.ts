
import { NgModule } from '@angular/core';
import {
  MatButtonModule, MatSelectModule,
  MatChipsModule, MatCardModule,
  MatIconModule, MatAutocompleteModule,
  MatInputModule, MatToolbarModule,
  MatCheckboxModule, MatDialogModule } from "@angular/material";

@NgModule({
  declarations: [],
  imports: [
    MatButtonModule,
    MatChipsModule,
    MatCardModule,
    MatIconModule,
    MatInputModule,
    MatToolbarModule,
    MatSelectModule,
    MatAutocompleteModule,
    MatInputModule,
    MatCheckboxModule,
    MatDialogModule,
  ],
  exports: [
    MatButtonModule,
    MatChipsModule,
    MatCardModule,
    MatIconModule,
    MatInputModule,
    MatToolbarModule,
    MatSelectModule,
    MatAutocompleteModule,
    MatInputModule,
    MatCheckboxModule,
    MatDialogModule,
  ],
})
export class AppMaterialModule { }
