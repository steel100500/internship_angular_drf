import { Component, OnInit, EventEmitter, Output } from '@angular/core';
import { HttpClient, HttpEventType, HttpHeaders } from '@angular/common/http';

@Component({
  selector: 'app-fileupload',
  templateUrl: './fileupload.component.html',
  styleUrls: ['./fileupload.component.css']
})
export class FileuploadComponent implements OnInit {
  baseurl = 'http://localhost:8002';

  fileData = null;
  url = '';
  constructor(private http: HttpClient) { }

  ngOnInit() {
  }

  @Output () urlEvent = new EventEmitter<string>()


  handleFileInput(event) {
    this.fileData = <File>event.target.files[0];
    var reader = new FileReader();
    reader.readAsDataURL(event.target.files[0]);
    reader.onload = (event) => { // called once readAsDataURL is completed
      this.url = event.target['result'];
    }
    let formData = new FormData();
    formData.append('file', this.fileData, this.fileData.name);
    // console.log('formData', formData);
    this.http.post(this.baseurl + '/file/upload/', formData, {
      reportProgress: true,
      observe: 'events',
    })
    .subscribe(events => {
      if(events.type == HttpEventType.UploadProgress) {
        console.log('Upload progress: ', Math.round(events.loaded / events.total * 100) + '%');
      } else if(events.type === HttpEventType.Response) {
        // console.log(events);
        const url = (''+events.url+events.body['file']).replace("/file/upload/","");
        // console.log('avatar_pre:',url)
        this.urlEvent.emit(url);
        // console.log();
      }
    })
    }



}
