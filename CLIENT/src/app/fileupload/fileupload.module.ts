import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { RouterModule } from '@angular/router';
import { FileuploadComponent } from './fileupload.component';
import { ProfileMaterialModule } from '../profile/profile-material.module';
import { MatFileUploadModule } from 'angular-material-fileupload';

@NgModule({
    imports: [
        CommonModule,
        ProfileMaterialModule,
        FormsModule,
        RouterModule,
        MatFileUploadModule
        ],
    declarations: [FileuploadComponent],
    exports: [FileuploadComponent],
    entryComponents: [FileuploadComponent],
    providers: []
})
export class FileUploadModule { }