import { Component, OnInit } from '@angular/core';
import { ProfileService } from './profile.service';
import { first } from 'rxjs/operators';
import { User } from '../user';
import { NewsService } from '../news/news.service';
import { Router } from '@angular/router';
import {MatDialog, MatDialogRef, MAT_DIALOG_DATA} from '@angular/material';
import { AddNewsComponent } from './addnews/addnews.component';
import { AuthService } from '../auth/auth.service';


@Component({
  selector: 'app-profile',
  templateUrl: './profile.component.html',
  styleUrls: [
    './profile.component.css',
    '../app.component.css'
  ],
  providers: [ProfileService]
})
export class ProfileComponent implements OnInit {
  users: any = [];
  // users: any = [];
  page: number = 1;
  news: any = [];
  selectedUser: User;
  public token: string;
  public usernameToken: string;
  userIDToken: number;
  public userName: any = [];
  public userId: number;
  private router: Router;
  statusSelected: boolean = false;
  public userAvatar: string;
  // сообщения об ошибках авторизации
  public errors: any = [];


  constructor(
    public auth: AuthService,
    private profileService: ProfileService,
    router: Router,
    public dialog: MatDialog
    ) {
      this.router = router;
  }
  receiveUserAvatar($event) {
    this.userAvatar = $event;
    this.users[0].avatar = $event;
    // console.log('users:', this.users[0]);
    this.profileService.updateUser(this.users[0]).subscribe(
      response => {
        console.log(response);
      },
      error => {
        this.errors = error['error'];
        console.log('error', this.errors);
      }
    );

  }

  ngOnInit() {
    this.token = localStorage.getItem('authToken');
    if (this.token == null) {
      this.router.navigate(['/auth/login']);
    }
    const token_parts = this.token.split(/\./);
    const token_decoded = JSON.parse(window.atob(token_parts[1]));

    // console.log(token_decoded);
    this.usernameToken = token_decoded.username;
    this.userIDToken = token_decoded.user_id;
    this.userId = Number(this.router.url.split(':')[1]);
    this.getUser();
    this.getUserNews();
  }

  //USER
  getUser() {
    this.profileService.getUser(this.userId)
    .pipe(first()).subscribe(response => {
      // console.log(response);
      this.users = [];
      this.users.push(response);
      // console.log('USERS', this.users);
      this.userName = this.users[0].username;
      this.userAvatar = this.users[0].avatar;
      // console.log('avatar:', this.userAvatar);
    },
    error => {
      this.errors = error['error'];
      console.log('error', this.errors);
    });
  }

  //NEWS
  getUserNews() {
    this.profileService.getUserNews()
    .pipe(first()).subscribe(response => {
      this.news = response['results'];
      this.news = this.news.filter(newsitem => {
        return newsitem.author === this.userId;
      });
      this.news = this.news.map(newsitem => {
        newsitem.author_id = this.userId;
        newsitem.author = this.userName;
        return newsitem;
      });
    },
    error => {
      this.errors = error['error'];
      console.log('error', this.errors);
    });
  }

  onSelect(user: User): void {
    this.statusSelected = !this.statusSelected;
    this.selectedUser = user;
    this.selectedUser.avatar = this.userAvatar;
  }

  updateUser() {
    this.profileService.updateUser(this.selectedUser).subscribe(
      response => {
        // console.log(response);
        this.getUser();
        this.statusSelected = !this.statusSelected;
      },
      error => {
        this.errors = error['error'];
        console.log('error', this.errors);
      }
    );
  }

  openDialog(): void {
    const dialogRef = this.dialog.open(
      AddNewsComponent,
      {width: '90%', disableClose: true, position: {top: '4%'}});
      dialogRef.afterClosed().subscribe(() => {
        // Do stuff after the dialog has closed
        this.getUserNews();
      });
    }
}