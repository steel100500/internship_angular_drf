import { Component, OnInit } from '@angular/core';
import { FormBuilder } from '@angular/forms';
import {MatDialogRef, MatDialog} from '@angular/material';
// import {AddNewsComponent} from '../addnews/addnews.component';

@Component({
  selector: 'app-canceladdnews',
  templateUrl: './canceladdnews.component.html',
  styleUrls: ['./canceladdnews.component.css']
})
export class CancelAddNewsComponent {

constructor(private fb: FormBuilder,
private dialog: MatDialog,
              private dialogRef: MatDialogRef<CancelAddNewsComponent>) {} // Closing dialog window

public cancel(): void { // To cancel the dialog window
this.dialogRef.close();
}

public cancelN(): void {
    this.dialog.closeAll();
}

}