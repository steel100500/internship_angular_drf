import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { RouterModule } from '@angular/router';
import { CancelAddNewsComponent } from './canceladdnews.component';
import { ProfileMaterialModule } from '../profile-material.module';

@NgModule({
    imports: [
        CommonModule,
        FormsModule,
        RouterModule,
        ProfileMaterialModule
        ],
    declarations: [CancelAddNewsComponent],
    exports: [],
    entryComponents: [CancelAddNewsComponent],
    providers: []
})
export class CancelAddNewsModule { }