import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CancelAddNewsComponent } from './canceladdnews.component';

describe('CanceladdnewsComponent', () => {
  let component: CancelAddNewsComponent;
  let fixture: ComponentFixture<CancelAddNewsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CancelAddNewsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CancelAddNewsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
