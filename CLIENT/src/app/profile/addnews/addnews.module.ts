import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { RouterModule } from '@angular/router';
import { TagsModule } from 'src/app/tags/tags.module';
import { AddNewsComponent } from './addnews.component';
import { ProfileMaterialModule } from '../profile-material.module';
import { FileUploadModule } from 'src/app/fileupload/fileupload.module';


@NgModule({
    imports: [
        CommonModule,
        FormsModule,
        RouterModule,
        TagsModule,
        ProfileMaterialModule,
        ReactiveFormsModule,
        FileUploadModule,
        ],
    declarations: [AddNewsComponent],
    exports: [],
    entryComponents: [AddNewsComponent],
    providers: []
})
export class AddNewsModule { }