import { Component, OnInit, VERSION, ViewChild, Output, EventEmitter } from '@angular/core';
import { MatDialog, MatDialogRef, MAT_DIALOG_DATA } from '@angular/material';
import { FormBuilder, FormControl, FormGroup, Validators } from '@angular/forms';
import { CancelAddNewsComponent } from '../canceladdnews/canceladdnews.component';
import { Router } from '@angular/router';
import { News } from 'src/app/news';
import { ProfileService } from '../profile.service';

@Component({
  selector: 'app-addnews',
  templateUrl: './addnews.component.html',
  styleUrls: ['./addnews.component.css']
})

export class AddNewsComponent implements OnInit {
  public breakpoint: number; // Breakpoint observer code
  public title: string = ``;
  public desc: string = ``;
  public author: number;
  public tags: string[];
  public image: string;
  public addCusForm: FormGroup;
  public errors: any = [];
  private router: Router;
  wasFormChanged = false;
  newNews: News;


  constructor(
    private fb: FormBuilder,
    public dialog: MatDialog,
    private profileService: ProfileService,
    router: Router,
  ) { this.router = router; }

  receiveTags($event) {
    this.tags = $event
  }
  receiveImage($event) {
    this.image = $event;
  }
  // @Output () urlEvent = new EventEmitter<string>()

  public ngOnInit(): void {
    console.log('TAGS', this.tags);
    this.author = Number(this.router.url.split(':')[1]);
    this.addCusForm = this.fb.group({
      title: [this.title, [Validators.required, Validators.pattern('a-zA-Z0-9 !.,_+-`~()+([a-zA-Z ]+)*')]],
      desc: [this.desc, [Validators.required]],
      author: [this.author, [Validators.required, Validators.pattern('[a-zA-Z0-9_.+-]+([a-zA-Z ]+)*')]],
      tags: [this.tags],
      image: [this.image],
    });

    console.log('addCusForm', this.addCusForm);
    this.breakpoint = window.innerWidth <= 600 ? 1 : 2; // Breakpoint observer code
  }

  public addNews(news: News) {
    this.addCusForm.value["tags"] = this.tags;
    this.addCusForm.value["image"] = this.image;
    this.newNews = this.addCusForm.value;
    console.log('addCusForm', this.addCusForm.value);
    this.profileService.addNews(this.newNews).subscribe(
      response => {
        console.log(response);
      },
      error => {
        this.errors = error['error'];
        console.log('error', this.errors);
      }
    );
    this.markAsDirty(this.addCusForm);
    this.router.navigate(['profile/:' + this.author + '/']);
    this.dialog.closeAll();
    // this.router.navigate(['../news']);
    // this.urlEvent.emit(url);
    // window.location.reload();
  }

  //additional window before cancel
  openDialog(): void {
    console.log(this.wasFormChanged);
    if(this.addCusForm.dirty) {
      const dialogRef = this.dialog.open(CancelAddNewsComponent, {
        width: '340px',
      });
    } else {
      this.dialog.closeAll();
    }
  }

  // tslint:disable-next-line:no-any
  public onResize(event: any): void {
    this.breakpoint = event.target.innerWidth <= 600 ? 1 : 2;
  }

  private markAsDirty(group: FormGroup): void {
    group.markAsDirty();
    // tslint:disable-next-line:forin
    for (const i in group.controls) {
      group.controls[i].markAsDirty();
    }
  }

  formChanged() {
    this.wasFormChanged = true;
  }

}