import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})

export class ProfileService {

  baseurl = 'http://localhost:8002';

  constructor(private http: HttpClient) {
   }

  getUser(userId) {
    return this.http.get(this.baseurl + '/users/'+ userId +'/', {
      headers: {
      Authorization: localStorage.getItem('authToken'),
      'Content-Type': 'application/json'
    }
  });
  }
  getUserNews() {
    return this.http.get(this.baseurl + '/news/', {
      headers: {
      Authorization: localStorage.getItem('authToken'),
      'Content-Type': 'application/json'
    }
  });
  }

  updateUser(userData): Observable<any> {
    console.log(userData);
    return this.http.put(this.baseurl + '/users/update-partial/' + userData.id + '/', userData, {
      headers: {
      Authorization: localStorage.getItem('authToken'),
      'Content-Type': 'application/json'
    }
  });
  }

  addNews(newsData): Observable<any> {
    console.log('newsData', newsData);
    return this.http.post(this.baseurl + '/news/', newsData);
  }

}
