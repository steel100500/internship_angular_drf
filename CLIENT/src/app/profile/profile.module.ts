import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { RouterModule } from '@angular/router';
import { AuthModule } from '../auth/auth.module';
import { TagsModule } from '../tags/tags.module';
import { ProfileMaterialModule } from './profile-material.module';
import { NewsModule } from '../news/news.module';
import { ProfileComponent } from './profile.component';
import { CancelAddNewsModule } from './canceladdnews/canceladdnews.module';
import { AddNewsModule } from './addnews/addnews.module';
import { FileUploadModule } from '../fileupload/fileupload.module';
import { NgxPaginationModule } from 'ngx-pagination';

@NgModule({
    imports: [
        CommonModule,
        ProfileMaterialModule,
        FormsModule,
        RouterModule,
        AuthModule,
        TagsModule,
        CancelAddNewsModule,
        AddNewsModule,
        NewsModule,
        FileUploadModule,
        NgxPaginationModule
        ],
    declarations: [ProfileComponent],
    exports: [ProfileMaterialModule],
    entryComponents: [ProfileComponent],
    providers: []
})
export class ProfileModule { }