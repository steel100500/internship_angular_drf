import { Injectable } from "@angular/core";
import { BehaviorSubject } from 'rxjs';
import { NgModule } from '@angular/core';


@Injectable({providedIn: 'root'})

export class FilterService {

    private messageSource = new BehaviorSubject<any>([]);
    currentMessage = this.messageSource.asObservable();

    constructor() { }

    changeMessage(message: any = []) {
        this.messageSource.next(message);
    }
}