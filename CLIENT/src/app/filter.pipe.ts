import { Pipe, PipeTransform, Output, EventEmitter, OnInit } from '@angular/core';
import { FilterService } from './filter.service';

@Pipe({
  name: 'filter'
})

export class FilterPipe implements PipeTransform, OnInit {

  message: any = [];

  constructor(private data: FilterService){ }

  ngOnInit() {
    this.data.currentMessage.subscribe(message=>this.message=message);
    // this.newMessage();
  }

  newMessage(items){
    // console.log('Changing...', items);
    this.data.changeMessage(items);
    // console.log('message',this.message);
  }

  transform(items: any[], searchText: string, field: string): any[] {
    if (!items) {
      return [];
    }
    if (!searchText) {
      return items;
    }

    searchText = searchText.toLocaleLowerCase();
    
    if (field === "all-0" || !field) {
      items = items.filter(item => {
        return JSON.stringify(item).toLocaleLowerCase().includes(searchText);
      });
      // console.log('PIPE',items);
      // this.sendNews(items);
      this.newMessage(items);
      return items;
    } else if (field === "tags-1") {
      items = items.filter(item => {
        if (item.tags != null){
          return item.tags.toString().toLocaleLowerCase().includes(searchText);
        }
      });
      // console.log('PIPE',items);
      // this.sendNews(items);
      this.newMessage(items);
      return items;
    } else if (field === "authors-2") {
      items = items.filter(item => {
        return item.author.includes(searchText);
      });
      // console.log('PIPE',items);
      // this.sendNews(items);
      this.newMessage(items);
      return items;
    }
  }
  // sendNews(items) {
  //   console.log("SEND NEWS");
  //   this.newsEvent.emit(items);
  // }

}
