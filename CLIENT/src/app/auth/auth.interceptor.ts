import {
  HttpInterceptor,
  HttpRequest,
  HttpHandler,
  HttpEvent,
  HttpResponse,
  HttpErrorResponse
} from '@angular/common/http';
import { AuthService } from './auth.service';
import { Observable } from 'rxjs';
import { Router } from '@angular/router';
import { Injectable } from '@angular/core';
import { tap } from 'rxjs/operators';


@Injectable()
export class AuthInterceptor implements HttpInterceptor {
 constructor(public auth: AuthService, private router: Router){}

 intercept(req: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>> {

   const authReq = req.clone({
    setHeaders: {
      Authorization: `${this.auth.getToken()}`,
    }
   });
   return next.handle(authReq).pipe(
     tap(event => {
       if (event instanceof HttpResponse)
          if(this.auth.getToken() == null){
            this.router.navigate(['/auth/login']);
          }
       }, err => {
       if (err instanceof HttpErrorResponse) {
         if(err.status == 401) console.log('Unauthorized');
       }
     })
   );
 }
}
