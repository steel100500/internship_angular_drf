import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { HttpClientModule } from '@angular/common/http';
import { RegistrationModule } from './registration/registration.module';
import { AuthService } from './auth.service';
import { FormsModule } from '@angular/forms';
import { RouterModule } from '@angular/router';
import { LoginModule } from './login/login.module';
import { LogoutComponent } from './logout.component';
import { SocialModule } from './social/social.module';


@NgModule({
  imports: [
    BrowserModule,
    HttpClientModule,
    RouterModule,
    RegistrationModule,
    LoginModule,
    FormsModule,
    SocialModule
  ],
  declarations: [ LogoutComponent, ],
  exports: [ LogoutComponent, ],
  entryComponents: [],
  providers: [AuthService],
})

export class AuthModule { }
