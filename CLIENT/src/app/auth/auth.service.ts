import {Injectable} from '@angular/core';
import {HttpClient, HttpHeaders, HttpRequest} from '@angular/common/http';
import { Observable } from 'rxjs';
import { Router } from '@angular/router';

@Injectable()
export class AuthService {

  cachedRequests: Array<HttpRequest<any>> = [];

  public collectFailedRequest(request): void {
      this.cachedRequests.push(request);
    }
  public retryFailedRequests(): void {
      // retry the requests. this method can
      // be called after the token is refreshed
    }
  baseurl = 'http://localhost:8002';

  private httpOptions: any;

  registerNewUser(userData): Observable<any> {
    return this.http.post(this.baseurl + '/users/', userData);
  }

  // текущий JWT токен
  public token: string;

  // время окончания жизни токена
  public token_expires: Date;

  // логин пользователя
  public username: string;

  // сообщения об ошибках авторизации
  public errors: any = [];
  public router: Router;

  constructor(private http: HttpClient, router: Router,) {
    this.router = router;
    this.httpOptions = {
      headers: new HttpHeaders({'Content-Type': 'application/json'})
    };
  }

  public login(user): Observable<any> {
    return this.http.post(this.baseurl + '/api-token-auth/', JSON.stringify(user), this.httpOptions);
  }

  public getToken(): string {
    return localStorage.getItem('authToken');
  }

  // обновление JWT токена
  public refreshToken() {
    this.http.post(this.baseurl + '/api-token-refresh/', JSON.stringify({token: this.token}), this.httpOptions).subscribe(
      data => {
        this.updateData(data['token']);
      },
      err => {
        this.errors = err['error'];
      }
    );
  }

  public logout() {
    localStorage.removeItem('authToken');
    this.router.navigate(['/auth/login']);
  }

  public updateData(token) {
    this.token = token;
    this.errors = [];

    // декодирование токена для получения логина и времени жизни токена
    const token_parts = this.token.split(/\./);
    const token_decoded = JSON.parse(window.atob(token_parts[1]));
    this.token_expires = new Date(token_decoded.exp * 1000);
    this.username = token_decoded.username;
  }


public jwtCheck(){
  try{
      const currentToken = localStorage.getItem('authToken');
      // console.log('currentToken', currentToken);
      if(currentToken){
        this.router.navigate(['../news']);
        // console.log('URL', this.router.url);
        // this.router.navigate(['..'+this.router.url]);
        return;
      }
      this.router.navigate(['/auth/login']);
  } catch(e){
      console.log('e',e);
  }
}
}
