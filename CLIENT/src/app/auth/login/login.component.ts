import {Component, OnInit} from '@angular/core';
import { AuthService } from '../auth.service';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {

  /**
   * An object representing the auth for the login form
   */
  public user: any;
  // сообщения об ошибках авторизации
  public error: string;
  public status: string;

  constructor(private authService: AuthService) { }

  ngOnInit() {
    this.user = {
      username: '',
      password: '',
    };
  }

  login() {
    this.authService.login(this.user).subscribe(
      data => {
        this.authService.updateData(data['token']);
        localStorage.setItem('authToken', data['token']);
        this.authService.router.navigate(['../news']);
      },
      err => {
        this.error = err['statusText'];
        this.status = err['status'];
        this.authService.errors = err['error'];
      }
    );
  }


  refreshToken() {
    this.authService.refreshToken();
  }

  logout() {
    this.authService.logout();
  }

  keytab(event) {
    let element = event.srcElement.nextElementSibling; // get the sibling element

    if(element == null)  // check if its null
        return;
    else
        element.focus();   // focus if not null
}
}
