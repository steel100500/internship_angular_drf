import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { LoginComponent } from './login.component';
import { LoginMaterialModule } from './login-material.module';
import { FormsModule } from '@angular/forms';
import { RouterModule } from '@angular/router';
import { SocialModule } from '../social/social.module';

@NgModule({
    imports: [
        CommonModule,
        LoginMaterialModule,
        FormsModule,
        RouterModule,
        SocialModule
    ],
    declarations: [LoginComponent],
    entryComponents: [LoginComponent]
})
export class LoginModule {}