import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { AuthService } from '../auth.service';

@Component({
  selector: 'app-registration',
  templateUrl: './registration.component.html',
  styleUrls: ['./registration.component.css']
})
export class RegistrationComponent implements OnInit {

  public register: any;
  // сообщения об ошибках авторизации
  public errors: any = [];

  constructor(
    private authService: AuthService,
    private router: Router,
    ) { }


  ngOnInit() {
    this.register = {
      first_name: '',
      last_name: '',
      username: '',
      password: '',
      email: '',
    };
  }


  registerUser() {
    this.authService.registerNewUser(this.register).subscribe(
      response => {
        // alert('Registration successful! ' + this.register.username + ' has been created');
        // this.router.navigate(['/auth/login']);
        console.log('Registration successful!');
        this.authService.login({'username': this.register.username, 'password': this.register.password})
      },
      error => {
        this.errors = error['error'];
        console.log('error', this.errors);
      }
    );
  }

}
