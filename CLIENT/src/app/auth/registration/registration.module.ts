import { NgModule } from '@angular/core';
import { RegistrationComponent } from './registration.component';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { RouterModule } from '@angular/router';
import { LoginMaterialModule } from '../login/login-material.module';

@NgModule({
    imports: [CommonModule, LoginMaterialModule, FormsModule, RouterModule],
    declarations: [RegistrationComponent],
    entryComponents: [RegistrationComponent]
})
export class RegistrationModule { }