
import { NgModule } from '@angular/core';
import {
  MatButtonModule, MatSelectModule,
  MatChipsModule, MatCardModule,
  MatIconModule, MatAutocompleteModule,
  MatInputModule, MatToolbarModule,
  MatCheckboxModule, MatDialogModule, MatFormFieldModule } from "@angular/material";

@NgModule({
  declarations: [],
  imports: [
    MatButtonModule,
    MatChipsModule,
    MatCardModule,
    MatIconModule,
    MatInputModule,
    MatToolbarModule,
    MatSelectModule,
    MatAutocompleteModule,
    MatInputModule,
    MatCheckboxModule,
    MatDialogModule,
    MatFormFieldModule,
  ],
  exports: [
    MatButtonModule,
    MatChipsModule,
    MatCardModule,
    MatIconModule,
    MatInputModule,
    MatToolbarModule,
    MatSelectModule,
    MatAutocompleteModule,
    MatInputModule,
    MatCheckboxModule,
    MatDialogModule,
    MatFormFieldModule,
  ],
})
export class RegistrationMaterialModule { }
