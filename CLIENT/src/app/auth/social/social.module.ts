import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { RouterModule } from '@angular/router';
import { AppMaterialModule } from 'src/app/app-material.module';
import { SocialComponent } from './social.component';

@NgModule({
    imports: [CommonModule, AppMaterialModule, FormsModule, RouterModule],
    declarations: [SocialComponent],
    entryComponents: [SocialComponent],
    exports: [SocialComponent],
})
export class SocialModule { }