import { Component, OnInit } from '@angular/core';
import { AuthService, GoogleLoginProvider} from 'angular-6-social-login';
import { Router } from '@angular/router';
import { AuthService as MyAuthService } from '../auth.service';
import { first } from 'rxjs/operators';

@Component({
  selector: 'app-social',
  templateUrl: './social.component.html',
  styleUrls: ['./social.component.css']
})

export class SocialComponent implements OnInit {

  private router: Router;
  userID: number;
  username: any;
  email: any;
  image: string;
  registerData = {
    first_name: '',
    last_name: '',
    username: '',
    password: '',
    email: '',
    avatar: '',
  };
  // сообщения об ошибках авторизации
  public errors: any = [];

  ngOnInit(): void {
  }

  constructor(
    private socialAuthService: AuthService,
    private authService: MyAuthService,
    router: Router,
    ) {
      this.router = router;
    }

  public socialSignIn(socialPlatform: string) {
    let socialPlatformProvider;
    if (socialPlatform == "google") {
      socialPlatformProvider = GoogleLoginProvider.PROVIDER_ID;
    }
    this.socialAuthService.signIn(socialPlatformProvider).then(
        (googleData) => {
          this.username = googleData.name;
          this.email = googleData.email;
          this.image = googleData.image;
          this.registerData.email = this.email;
          this.registerData.username = this.email;
          this.registerData.password = '' + googleData.id;
          this.registerData.avatar = '' + googleData.image;
          this.authService.registerNewUser(this.registerData)
            .pipe(first()).subscribe(
              (response) => {
              this.authService.login({'username': this.registerData.username, 'password': this.registerData.password}).subscribe(
                data => {
                  this.authService.updateData(data['token']);
                  localStorage.setItem('authToken', data['token']);
                  this.authService.router.navigate(['../news']);
                },
                err => {
                  this.authService.errors = err['error'];
                }
              );
            },
            (err) => {
              this.errors = err['error'];
              console.log('error', this.errors);
            }
          );
          this.authService.login({'username': this.registerData.username, 'password': this.registerData.password}).subscribe(
            data => {
              this.authService.updateData(data['token']);
              localStorage.setItem('authToken', data['token']);
              this.authService.router.navigate(['../news']);
            },
            err => {
              this.authService.errors = err['error'];
            }
          );
        }
      );
  }
}