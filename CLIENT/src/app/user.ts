export class User {
    id: number;
    username: string;
    password: string;
    email: string;
    first_name: string;
    last_name: string;
    is_active: boolean;
    avatar: string;
}