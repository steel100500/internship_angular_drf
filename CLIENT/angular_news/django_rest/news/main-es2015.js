(window["webpackJsonp"] = window["webpackJsonp"] || []).push([["main"],{

/***/ "./$$_lazy_route_resource lazy recursive":
/*!******************************************************!*\
  !*** ./$$_lazy_route_resource lazy namespace object ***!
  \******************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

function webpackEmptyAsyncContext(req) {
	// Here Promise.resolve().then() is used instead of new Promise() to prevent
	// uncaught exception popping up in devtools
	return Promise.resolve().then(function() {
		var e = new Error("Cannot find module '" + req + "'");
		e.code = 'MODULE_NOT_FOUND';
		throw e;
	});
}
webpackEmptyAsyncContext.keys = function() { return []; };
webpackEmptyAsyncContext.resolve = webpackEmptyAsyncContext;
module.exports = webpackEmptyAsyncContext;
webpackEmptyAsyncContext.id = "./$$_lazy_route_resource lazy recursive";

/***/ }),

/***/ "./node_modules/raw-loader/index.js!./src/app/app.component.html":
/*!**************************************************************!*\
  !*** ./node_modules/raw-loader!./src/app/app.component.html ***!
  \**************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<!--The content below is only a placeholder and can be replaced.-->\n<router-outlet></router-outlet>"

/***/ }),

/***/ "./node_modules/raw-loader/index.js!./src/app/auth/login/login.component.html":
/*!***************************************************************************!*\
  !*** ./node_modules/raw-loader!./src/app/auth/login/login.component.html ***!
  \***************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<mat-toolbar color=\"primary\">\n    <h1>Login</h1>\n</mat-toolbar>\n  <h2>Log In</h2>\n  <div class=\"example-container\">\n    <div class=\"col-sm-4\">\n      <mat-form-field class=\"example-full-width\">\n        <input matInput placeholder=\"Login\" (keyup.enter)=\"keytab($event)\" type=\"text\" name=\"login-username\" [(ngModel)]=\"user.username\">\n        <span *ngFor=\"let error of authService.errors.username\"><br />\n        {{ error }}</span>\n      </mat-form-field>\n    </div>\n    <div class=\"col-sm-4\">\n      <mat-form-field class=\"example-full-width\">\n        <input matInput placeholder=\"Password\" (keyup.enter)=\"login()\" type=\"password\" name=\"login-password\" [(ngModel)]=\"user.password\">\n        <span *ngFor=\"let error of authService.errors.password\"><br />\n        {{ error }}</span>\n      </mat-form-field>\n    </div>\n    <div class=\"col-sm-4\">\n      <button (click)=\"login()\" mat-raised-button color=\"primary\" class=\"btn btn-primary\">Log In</button>\n      <a [routerLink]=\"['/auth/registration']\" class=\"btn btn-link\">Sign UP</a>\n    </div>\n    <div class=\"col-sm-12\">\n      <span *ngFor=\"let error of authService.errors.non_field_errors\">{{ error }}<br /></span>\n    </div>\n  </div>\n<app-social></app-social>"

/***/ }),

/***/ "./node_modules/raw-loader/index.js!./src/app/auth/logout.component.html":
/*!**********************************************************************!*\
  !*** ./node_modules/raw-loader!./src/app/auth/logout.component.html ***!
  \**********************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<button\n(click)=\"logout()\"\n mat-raised-button color=\"primary\"\n class=\"btn btn-primary\">Log Out</button>"

/***/ }),

/***/ "./node_modules/raw-loader/index.js!./src/app/auth/registration/registration.component.html":
/*!*****************************************************************************************!*\
  !*** ./node_modules/raw-loader!./src/app/auth/registration/registration.component.html ***!
  \*****************************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<mat-toolbar color=\"primary\">\n    <h1>Registration</h1>\n</mat-toolbar>\n\n<div class=\"col-sm-12\">\n    <mat-form-field class=\"example-full-width\">\n        <input matInput placeholder=\"FirstName\" type=\"text\" name=\"registration-firstname\" [(ngModel)]=\"register.first_name\">\n        <span *ngFor=\"let error of errors.first_name\"><br />\n            {{ error }}</span>\n    </mat-form-field>\n    <mat-form-field class=\"example-full-width\">\n        <input matInput placeholder=\"LastName\" type=\"text\" name=\"registration-lastname\" [(ngModel)]=\"register.last_name\">\n        <span *ngFor=\"let error of errors.last_name\"><br />\n            {{ error }}</span>\n    </mat-form-field>\n    <mat-form-field class=\"example-full-width\">\n        <input matInput placeholder=\"Login\" type=\"text\" name=\"registration-username\" [(ngModel)]=\"register.username\">\n        <span *ngFor=\"let error of errors.username\"><br />\n            {{ error }}</span>\n    </mat-form-field>\n    <mat-form-field class=\"example-full-width\">\n        <input matInput placeholder=\"Password\" type=\"password\" name=\"registration-password\" [(ngModel)]=\"register.password\">\n        <span *ngFor=\"let error of errors.password\"><br />\n            {{ error }}</span>\n    </mat-form-field>\n    <mat-form-field class=\"example-full-width\">\n        <input matInput placeholder=\"Email\" (keyup.enter)=\"registerUser()\" type=\"email\" name=\"registration-email\" [(ngModel)]=\"register.email\">\n        <span *ngFor=\"let error of errors.email\"><br />\n            {{ error }}</span>\n    </mat-form-field>\n        <button (click)=\"registerUser()\" mat-raised-button color=\"primary\" class=\"btn btn-primary\">Sign UP</button>\n        <a [routerLink]=\"['/auth/login']\" class=\"btn btn-link\">Cancel</a>\n</div>\n"

/***/ }),

/***/ "./node_modules/raw-loader/index.js!./src/app/auth/social/social.component.html":
/*!*****************************************************************************!*\
  !*** ./node_modules/raw-loader!./src/app/auth/social/social.component.html ***!
  \*****************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<div class=\"jumbotron bg-transparent text-center\">\n    <div *ngIf=\"!user\" class=\"card text-center\">\n      <h6 class=\"card-header\">\n        Social Login\n      </h6>\n      <div class=\"card-block\">\n        <h4 class=\"card-title\">Not signed in</h4>\n        <p class=\"card-text\">Sign in with</p>\n      </div>\n      <div class=\"card-block\">\n        <button class=\"btn btn-social-icon btn-google mx-1\" (click)=\"signInWithGoogle()\"><span class=\"fa fa-google\"></span></button>\n        <button class=\"btn btn-social-icon btn-facebook mx-1\" (click)=\"signInWithFB()\"><span class=\"fa fa-facebook\"></span></button>\n        <button class=\"btn btn-social-icon btn-linkedin mx-1\" (click)=\"signInWithLinkedIn()\" disabled><span class=\"fa fa-linkedin\"></span></button>\n      </div>\n    </div>\n    <div *ngIf=\"user\" class=\"card text-center\">\n      <h6 class=\"card-header\">\n        Social Login\n      </h6>\n      <div class=\"card-block\"></div>\n      <img class=\"card-img-top img-responsive photo\" src=\"{{ user.photoUrl }}\">\n      <div class=\"card-block\">\n        <h4 class=\"card-title\">{{ user.name }}</h4>\n        <p class=\"card-text\">{{ user.email }}</p>\n      </div>\n      <div class=\"card-block\">\n        <button class=\"btn btn-danger\" (click)=\"signOut()\">Sign out</button>\n      </div>\n    </div>\n  </div>"

/***/ }),

/***/ "./node_modules/raw-loader/index.js!./src/app/fileupload/fileupload.component.html":
/*!********************************************************************************!*\
  !*** ./node_modules/raw-loader!./src/app/fileupload/fileupload.component.html ***!
  \********************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<div class=\"container\">\n    <div class=\"row\">\n        <div class=\"col-md-6 offset-md-3\">\n            <h3>Choose File</h3>\n            <form (ngSubmit)=\"onSubmit()\">\n                <div class=\"form-group\">\n                    <img [src]=\"url\" height=\"200\"> <br/>\n                    <input type=\"file\" name=\"image\"  accept=\"image/*\" (change)=\"handleFileInput($event)\"  />\n                </div>\n                <!-- <div class=\"form-group\">\n                    <button class=\"btn btn-primary\">Submit image</button>\n                </div> -->\n            </form>\n        </div>\n    </div>\n</div>"

/***/ }),

/***/ "./node_modules/raw-loader/index.js!./src/app/news/news.component.html":
/*!********************************************************************!*\
  !*** ./node_modules/raw-loader!./src/app/news/news.component.html ***!
  \********************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<mat-toolbar color=\"primary\">\n  <span>News </span>\n  <a mat-button routerLink=\"/users\" class=\"\">Users</a>\n  <a mat-button (click)=\"getUserPage()\" class=\"\">Profile</a>\n  <app-logout></app-logout>\n</mat-toolbar>\n<div style=\"text-align:center\">\n</div>\n  <!-- Search form -->\n  <form class=\"form-inline waves-light\" mdbWavesEffect>\n    <mat-form-field class=\"example-full-width\">\n      <input matInput placeholder=\"Login\" type=\"search\" [(ngModel)]=\"searchText\" [ngModelOptions]=\"{standalone: true}\" placeholder=\"Search title\">\n    </mat-form-field>\n  </form>\n  <mat-form-field>\n      <mat-label>Favorite filter</mat-label>\n      <mat-select [(ngModel)]=\"filterField\">\n        <mat-option *ngFor=\"let filter of filters\" [value]=\"filter.value\">\n          {{filter.viewValue}}\n        </mat-option>\n      </mat-select>\n  </mat-form-field>\n\n  <h1 class=\"text-center\">What's in the news today?</h1>\n  <ul>\n    <li *ngFor=\"let itemnews of pageOfItems | filter:searchText:filterField\">\n      <h3>{{ itemnews.title }}</h3>\n      <p *ngIf=\"itemnews.desc\">\n        {{ itemnews.desc }}\n      </p>\n      <p *ngIf=\"itemnews.author\">\n        <span>Author: </span>\n        <a mat-button (click)=\"getUserPage(itemnews.author_id)\" class=\"pirple\">{{ itemnews.author }}</a>\n      </p>\n      <p *ngIf=\"itemnews.tags\">\n        <span>Tags: </span>\n        {{ itemnews.tags }}\n      </p>\n      <!-- <app-tags></app-tags> -->\n    </li>\n\n  </ul>\n  <div class=\"card text-center m-3\">\n      <div class=\"card-footer pb-0 pt-3\">\n          <jw-pagination [items]=\"news\" (newsEvent)=\"receiveNews($event)\" (changePage)=\"onChangePage($event)\"></jw-pagination>\n      </div>\n  </div>\n\n"

/***/ }),

/***/ "./node_modules/raw-loader/index.js!./src/app/profile/addnews/addnews.component.html":
/*!**********************************************************************************!*\
  !*** ./node_modules/raw-loader!./src/app/profile/addnews/addnews.component.html ***!
  \**********************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<div>\n    <div class=\"add-title\">\n      <h2 class=\"primary\">Add news</h2>\n    </div>\n    <div>\n      <form [formGroup]=\"addCusForm\" (change)=\"formChanged()\">\n        <app-fileupload (urlEvent)=\"receiveImage($event)\"></app-fileupload>\n        <mat-grid-list cols=\"2\" [cols]=\"breakpoint\" rowHeight=\"85px\" (window:resize)=\"onResize($event)\">\n          <mat-grid-tile>\n            <div class=\"text-inside\">\n              <mat-form-field>\n                <input matInput placeholder=\"Title\" value={{title}} formControlName=\"title\" required>\n                <mat-error *ngIf=\"addCusForm.controls.title.hasError('required')\">\n                  Please enter Title\n                </mat-error>\n                <mat-error *ngIf=\"addCusForm.controls.title.hasError('pattern')\">\n                  Please enter valid Title\n                </mat-error>\n              </mat-form-field>\n            </div>\n          </mat-grid-tile>\n          <mat-grid-tile>\n            <div class=\"text-inside\">\n              <mat-form-field class=\"example-full-width\">\n                <textarea matInput placeholder=\"Description\" value={{desc}} formControlName=\"desc\" required></textarea>\n                <mat-error *ngIf=\"addCusForm.controls.desc.hasError('required')\">\n                  Please enter Description\n                </mat-error>\n                <mat-error *ngIf=\"addCusForm.controls.desc.hasError('pattern')\">\n                  Please enter valid Description\n                </mat-error>\n              </mat-form-field>\n            </div>\n          </mat-grid-tile>\n          <mat-grid-tile>\n            <mat-form-field class=\"example-full-width\">\n              <input matInput placeholder=\"Author\"\n                    value={{author}}\n                    formControlName=\"author\"\n                    required [readonly]=\"true\">\n            </mat-form-field>\n          </mat-grid-tile>\n          <mat-grid-tile>\n            <app-tags (tagsEvent)=\"receiveTags($event)\"></app-tags>\n          </mat-grid-tile>\n          <!-- <mat-grid-tile>\n            <mat-form-field class=\"example-full-width\">\n              <app-file-upload formControlName=\"image\" [progress]=\"progress\"></app-file-upload>\n            </mat-form-field>\n          </mat-grid-tile> -->\n        </mat-grid-list>\n\n        <div class=\"btn-sec\">\n          <button mat-raised-button type=\"submit\" color=\"primary\" class=\"Update-btn\" (click)=\"addNews()\">Save</button>\n          <button mat-raised-button type=\"button\" class=\"Discard-btn\" (click)=\"openDialog()\">Cancel</button>\n        </div>\n\n      </form>\n    </div>\n  </div>\n  "

/***/ }),

/***/ "./node_modules/raw-loader/index.js!./src/app/profile/canceladdnews/canceladdnews.component.html":
/*!**********************************************************************************************!*\
  !*** ./node_modules/raw-loader!./src/app/profile/canceladdnews/canceladdnews.component.html ***!
  \**********************************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<h3>Are you sure you want to discard the changes?</h3>\n<div>\n  <button mat-raised-button type=\"button\" class=\"Delete-btn\" (click)=\"cancelN()\">Discard</button>\n  <button mat-raised-button type=\"submit\" color=\"primary\" class=\"Cancel-btn\" (click)=\"cancel()\">Cancel</button>\n</div>"

/***/ }),

/***/ "./node_modules/raw-loader/index.js!./src/app/profile/profile.component.html":
/*!**************************************************************************!*\
  !*** ./node_modules/raw-loader!./src/app/profile/profile.component.html ***!
  \**************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<!-- <p>profile works!</p> -->\n<mat-toolbar color=\"primary\">\n  <h1>Profile</h1>\n  <a mat-button routerLink=\"/news\" class=\"\">News</a>\n  <app-logout></app-logout>\n</mat-toolbar>\n<div class=\"row\">\n  <div style=\"text-align:center\" class=\"col-sm-4\">\n    <ul class=\"col-9 users\">\n      <div *ngFor=\"let user of users\"\n        [class.selected]=\"user === selectedUser\">\n        <h3 class=\"text-center\">{{user.username}} Settings</h3>\n        <!-- <img src=\"http://localhost:8002/media/static/DSC_1055_SRL9lw3.png\" height=\"200\"> <br/> -->\n        <img [src]=\"user.avatar\" height=\"200\">\n        {{user.avatar}}\n        <!-- <app-fileupload (urlEvent)=\"receiveUserAvatar($event)\"></app-fileupload>\n        <div *ngFor=\"let error of errors.password\" class=\"alert alert-danger\" role=\"alert\">\n          PASSWORD: {{ error }}\n        </div> -->\n        <ul type=\"none\">\n          <li><label>username: </label>{{user.username}}</li>\n          <li><label>password: </label>{{user.password}}</li>\n          <li><label>email: </label>{{user.email}}</li>\n          <li><label>first_name: </label>{{user.first_name}}</li>\n          <li><label>last_name: </label>{{user.last_name}}</li>\n        </ul>\n        <button *ngIf=\"user.id === userIDToken\" (click)=\"onSelect(user)\" mat-raised-button color=\"primary\" class=\"btn btn-primary\">Edit profile</button>\n      </div>\n    </ul>\n    <div class=\"col-3\" *ngIf=\"statusSelected\">\n      <h2>{{selectedUser.username | uppercase}} Details</h2>\n      <div>\n        <span>id: </span>{{selectedUser.id}}\n      </div>\n      <app-fileupload (urlEvent)=\"receiveUserAvatar($event)\"></app-fileupload>\n      <label>avatar:\n        <input [(ngModel)]=\"selectedUser.avatar\" placeholder=\"avatar\"/>\n        <span *ngFor=\"let error of errors.avatar\"><br/>\n          {{ error }}</span>\n      </label><br>\n      <label>username:\n        <input [(ngModel)]=\"selectedUser.username\" placeholder=\"username\"/>\n        <span *ngFor=\"let error of errors.username\"><br/>\n          {{ error }}</span>\n      </label><br>\n      <label>password:\n        <input [(ngModel)]=\"selectedUser.password\" placeholder=\"password\"/>\n          <div *ngFor=\"let error of errors.password\" class=\"alert alert-danger\" role=\"alert\">\n            {{ error }}\n          </div>\n      </label><br>\n      <label>email:\n        <input [(ngModel)]=\"selectedUser.email\" placeholder=\"email\"/>\n        <span *ngFor=\"let error of errors.email\"><br />\n          {{ error }}</span>\n      </label><br>\n      <label>first_name:\n        <input [(ngModel)]=\"selectedUser.first_name\" placeholder=\"first_name\"/>\n        <span *ngFor=\"let error of errors.first_name\"><br />\n          {{ error }}</span>\n      </label><br>\n      <label>last_name:\n        <input [(ngModel)]=\"selectedUser.last_name\" placeholder=\"last_name\"/>\n        <span *ngFor=\"let error of errors.last_name\"><br />\n          {{ error }}</span>\n      </label><br>\n      <!-- <label>is_active:\n          <mat-checkbox [(ngModel)]=\"selectedUser.is_active\"></mat-checkbox>\n      </label><br> -->\n      <button (click)=\"updateUser()\" mat-raised-button color=\"primary\" class=\"btn btn-primary\">SAVE</button>\n    </div>\n  </div>\n\n  <div class=\"col-sm-8\">\n    <h3 class=\"text-center\">Personal News</h3>\n    <!-- <button mat-raised-button (click)=\"openDialog()\">Create news</button> -->\n    <button *ngIf=\"userId === userIDToken\"\n            (click)=\"openDialog()\"\n            mat-raised-button color=\"primary\"\n            class=\"btn btn-primary\">Create NEWS\n    </button>\n    <ul>\n      <li *ngFor=\"let itemnews of pageOfItems\">\n        <h3>{{ itemnews.title }}</h3>\n        <img [src]=\"itemnews.image\" height=\"200\">\n        <p *ngIf=\"itemnews.desc\">\n          {{ itemnews.desc }}\n        </p>\n        <p *ngIf=\"itemnews.author\">\n          <span>Author: </span>{{ itemnews.author }}\n          <span>AuthorID: </span>{{ itemnews.author_id }}\n        </p>\n        <p *ngIf=\"itemnews.tags\">\n          {{ itemnews.tags }}\n        </p>\n        <app-tags></app-tags>\n      </li>\n    </ul>\n    <div class=\"card text-center m-3\">\n      <div class=\"card-footer pb-0 pt-3\">\n        <jw-pagination [items]=\"news\" (changePage)=\"onChangePage($event)\"></jw-pagination>\n      </div>\n    </div>\n  </div>\n</div>\n\n"

/***/ }),

/***/ "./node_modules/raw-loader/index.js!./src/app/tags/tags.component.html":
/*!********************************************************************!*\
  !*** ./node_modules/raw-loader!./src/app/tags/tags.component.html ***!
  \********************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<!-- <p>tags works!</p> -->\n<mat-form-field class=\"example-chip-list\">\n    <mat-chip-list #chipList aria-label=\"Tag selection\">\n      <mat-chip\n        *ngFor=\"let tag of tags\"\n        [selectable]=\"selectable\"\n        [removable]=\"removable\"\n        (removed)=\"remove(tag)\">\n        {{tag}}\n        <mat-icon matChipRemove *ngIf=\"removable\">cancel</mat-icon>\n      </mat-chip>\n      <input\n        placeholder=\"New tag...\"\n        #tagInput\n        [formControl]=\"tagCtrl\"\n        [matAutocomplete]=\"auto\"\n        [matChipInputFor]=\"chipList\"\n        [matChipInputSeparatorKeyCodes]=\"separatorKeysCodes\"\n        [matChipInputAddOnBlur]=\"addOnBlur\"\n        (matChipInputTokenEnd)=\"add($event)\">\n    </mat-chip-list>\n    <mat-autocomplete #auto=\"matAutocomplete\" (optionSelected)=\"selected($event)\">\n      <mat-option *ngFor=\"let tag of filteredTags | async\" [value]=\"tag\">\n        {{tag}}\n      </mat-option>\n    </mat-autocomplete>\n</mat-form-field>\n{{tags}}"

/***/ }),

/***/ "./node_modules/raw-loader/index.js!./src/app/users/users.component.html":
/*!**********************************************************************!*\
  !*** ./node_modules/raw-loader!./src/app/users/users.component.html ***!
  \**********************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<mat-toolbar color=\"primary\">\n  <span>Users</span>\n  <a mat-button routerLink=\"/news\" class=\"\">News</a>\n  <app-logout></app-logout>\n</mat-toolbar>\n\n<ul class=\"col-9\">\n    <h2>Users</h2>\n    <ul class=\"users\">\n      <li *ngFor=\"let user of pageOfItems\"\n        [class.selected]=\"user === selectedUser\"\n        (click)=\"onSelect(user)\">\n        <span class=\"badge\">{{user.id}}</span> {{user.username}}\n      </li>\n</ul>\n<div class=\"col-3\" *ngIf=\"selectedUser\">\n    <h2>{{selectedUser.username | uppercase}} Details</h2>\n    <div><span>id: </span>{{selectedUser.id}}</div>\n    <div>\n      <label>username:\n        <input [(ngModel)]=\"selectedUser.username\" placeholder=\"username\"/>\n      </label><br>\n      <label>password:\n        <input [(ngModel)]=\"selectedUser.password\" placeholder=\"password\"/>\n      </label><br>\n      <label>email:\n        <input [(ngModel)]=\"selectedUser.email\" placeholder=\"email\"/>\n      </label><br>\n      <label>first_name:\n        <input [(ngModel)]=\"selectedUser.first_name\" placeholder=\"first_name\"/>\n      </label><br>\n      <label>last_name:\n        <input [(ngModel)]=\"selectedUser.last_name\" placeholder=\"last_name\"/>\n      </label><br>\n      <label>is_active:\n          <mat-checkbox [(ngModel)]=\"selectedUser.is_active\"></mat-checkbox>\n      </label><br>\n    </div>\n</div>\n\n<div class=\"card text-center m-3\">\n    <div class=\"card-footer pb-0 pt-3\">\n        <jw-pagination [items]=\"users\" (changePage)=\"onChangePage($event)\"></jw-pagination>\n    </div>\n</div>\n"

/***/ }),

/***/ "./src/app/app-material.module.ts":
/*!****************************************!*\
  !*** ./src/app/app-material.module.ts ***!
  \****************************************/
/*! exports provided: AppMaterialModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "AppMaterialModule", function() { return AppMaterialModule; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm2015/core.js");
/* harmony import */ var _angular_material__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/material */ "./node_modules/@angular/material/esm2015/material.js");



let AppMaterialModule = class AppMaterialModule {
};
AppMaterialModule = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
        declarations: [],
        imports: [
            _angular_material__WEBPACK_IMPORTED_MODULE_2__["MatButtonModule"],
            _angular_material__WEBPACK_IMPORTED_MODULE_2__["MatChipsModule"],
            _angular_material__WEBPACK_IMPORTED_MODULE_2__["MatCardModule"],
            _angular_material__WEBPACK_IMPORTED_MODULE_2__["MatIconModule"],
            _angular_material__WEBPACK_IMPORTED_MODULE_2__["MatInputModule"],
            _angular_material__WEBPACK_IMPORTED_MODULE_2__["MatToolbarModule"],
            _angular_material__WEBPACK_IMPORTED_MODULE_2__["MatSelectModule"],
            _angular_material__WEBPACK_IMPORTED_MODULE_2__["MatAutocompleteModule"],
            _angular_material__WEBPACK_IMPORTED_MODULE_2__["MatInputModule"],
            _angular_material__WEBPACK_IMPORTED_MODULE_2__["MatCheckboxModule"],
            _angular_material__WEBPACK_IMPORTED_MODULE_2__["MatDialogModule"],
        ],
        exports: [
            _angular_material__WEBPACK_IMPORTED_MODULE_2__["MatButtonModule"],
            _angular_material__WEBPACK_IMPORTED_MODULE_2__["MatChipsModule"],
            _angular_material__WEBPACK_IMPORTED_MODULE_2__["MatCardModule"],
            _angular_material__WEBPACK_IMPORTED_MODULE_2__["MatIconModule"],
            _angular_material__WEBPACK_IMPORTED_MODULE_2__["MatInputModule"],
            _angular_material__WEBPACK_IMPORTED_MODULE_2__["MatToolbarModule"],
            _angular_material__WEBPACK_IMPORTED_MODULE_2__["MatSelectModule"],
            _angular_material__WEBPACK_IMPORTED_MODULE_2__["MatAutocompleteModule"],
            _angular_material__WEBPACK_IMPORTED_MODULE_2__["MatInputModule"],
            _angular_material__WEBPACK_IMPORTED_MODULE_2__["MatCheckboxModule"],
            _angular_material__WEBPACK_IMPORTED_MODULE_2__["MatDialogModule"],
        ],
    })
], AppMaterialModule);



/***/ }),

/***/ "./src/app/app-routing.module.ts":
/*!***************************************!*\
  !*** ./src/app/app-routing.module.ts ***!
  \***************************************/
/*! exports provided: AppRoutingModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "AppRoutingModule", function() { return AppRoutingModule; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm2015/core.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm2015/router.js");
/* harmony import */ var _auth_auth_routing__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ./auth/auth.routing */ "./src/app/auth/auth.routing.ts");
/* harmony import */ var _news_news_component__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ./news/news.component */ "./src/app/news/news.component.ts");
/* harmony import */ var _users_users_component__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ./users/users.component */ "./src/app/users/users.component.ts");
/* harmony import */ var _profile_profile_component__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ./profile/profile.component */ "./src/app/profile/profile.component.ts");







const routes = [
    ..._auth_auth_routing__WEBPACK_IMPORTED_MODULE_3__["authRoutes"],
    { path: '',
        children: [
            { path: 'news', component: _news_news_component__WEBPACK_IMPORTED_MODULE_4__["NewsComponent"] },
            { path: 'users', component: _users_users_component__WEBPACK_IMPORTED_MODULE_5__["UsersComponent"] },
            { path: 'profile/:id', component: _profile_profile_component__WEBPACK_IMPORTED_MODULE_6__["ProfileComponent"] },
        ]
    },
];
let AppRoutingModule = class AppRoutingModule {
};
AppRoutingModule = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
        imports: [_angular_router__WEBPACK_IMPORTED_MODULE_2__["RouterModule"].forRoot(routes)],
        exports: [_angular_router__WEBPACK_IMPORTED_MODULE_2__["RouterModule"]]
    })
], AppRoutingModule);



/***/ }),

/***/ "./src/app/app.component.css":
/*!***********************************!*\
  !*** ./src/app/app.component.css ***!
  \***********************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "a:focus, a:hover {\n  color: #ffffff;\n  text-decoration: none;\n}\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbInNyYy9hcHAvYXBwLmNvbXBvbmVudC5jc3MiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IkFBQUE7RUFDRSxjQUFjO0VBQ2QscUJBQXFCO0FBQ3ZCIiwiZmlsZSI6InNyYy9hcHAvYXBwLmNvbXBvbmVudC5jc3MiLCJzb3VyY2VzQ29udGVudCI6WyJhOmZvY3VzLCBhOmhvdmVyIHtcbiAgY29sb3I6ICNmZmZmZmY7XG4gIHRleHQtZGVjb3JhdGlvbjogbm9uZTtcbn0iXX0= */"

/***/ }),

/***/ "./src/app/app.component.ts":
/*!**********************************!*\
  !*** ./src/app/app.component.ts ***!
  \**********************************/
/*! exports provided: AppComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "AppComponent", function() { return AppComponent; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm2015/core.js");
/* harmony import */ var _auth_auth_service__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ./auth/auth.service */ "./src/app/auth/auth.service.ts");



let AppComponent = class AppComponent {
    constructor(authService) {
        this.authService = authService;
    }
    ngOnInit() {
        this.authService.jwtCheck();
    }
};
AppComponent.ctorParameters = () => [
    { type: _auth_auth_service__WEBPACK_IMPORTED_MODULE_2__["AuthService"] }
];
AppComponent = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
        selector: 'app-root',
        template: __webpack_require__(/*! raw-loader!./app.component.html */ "./node_modules/raw-loader/index.js!./src/app/app.component.html"),
        providers: [],
        styles: [__webpack_require__(/*! ./app.component.css */ "./src/app/app.component.css")]
    })
], AppComponent);



/***/ }),

/***/ "./src/app/app.module.ts":
/*!*******************************!*\
  !*** ./src/app/app.module.ts ***!
  \*******************************/
/*! exports provided: provideConfig, AppModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "provideConfig", function() { return provideConfig; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "AppModule", function() { return AppModule; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_platform_browser__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/platform-browser */ "./node_modules/@angular/platform-browser/fesm2015/platform-browser.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm2015/core.js");
/* harmony import */ var _angular_common_http__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/common/http */ "./node_modules/@angular/common/fesm2015/http.js");
/* harmony import */ var _app_routing_module__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ./app-routing.module */ "./src/app/app-routing.module.ts");
/* harmony import */ var _app_component__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ./app.component */ "./src/app/app.component.ts");
/* harmony import */ var _angular_platform_browser_animations__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! @angular/platform-browser/animations */ "./node_modules/@angular/platform-browser/fesm2015/animations.js");
/* harmony import */ var _auth_auth_module__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! ./auth/auth.module */ "./src/app/auth/auth.module.ts");
/* harmony import */ var _app_material_module__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(/*! ./app-material.module */ "./src/app/app-material.module.ts");
/* harmony import */ var _news_news_module__WEBPACK_IMPORTED_MODULE_9__ = __webpack_require__(/*! ./news/news.module */ "./src/app/news/news.module.ts");
/* harmony import */ var _users_users_module__WEBPACK_IMPORTED_MODULE_10__ = __webpack_require__(/*! ./users/users.module */ "./src/app/users/users.module.ts");
/* harmony import */ var _profile_profile_module__WEBPACK_IMPORTED_MODULE_11__ = __webpack_require__(/*! ./profile/profile.module */ "./src/app/profile/profile.module.ts");
/* harmony import */ var _auth_token_interceptor__WEBPACK_IMPORTED_MODULE_12__ = __webpack_require__(/*! ./auth/token.interceptor */ "./src/app/auth/token.interceptor.ts");
/* harmony import */ var angularx_social_login__WEBPACK_IMPORTED_MODULE_13__ = __webpack_require__(/*! angularx-social-login */ "./node_modules/angularx-social-login/angularx-social-login.js");
/* harmony import */ var _auth_social_social_module__WEBPACK_IMPORTED_MODULE_14__ = __webpack_require__(/*! ./auth/social/social.module */ "./src/app/auth/social/social.module.ts");
















const config = new angularx_social_login__WEBPACK_IMPORTED_MODULE_13__["AuthServiceConfig"]([
    {
        id: angularx_social_login__WEBPACK_IMPORTED_MODULE_13__["GoogleLoginProvider"].PROVIDER_ID,
        provider: new angularx_social_login__WEBPACK_IMPORTED_MODULE_13__["GoogleLoginProvider"]('624796833023-clhjgupm0pu6vgga7k5i5bsfp6qp6egh.apps.googleusercontent.com')
    },
    {
        id: angularx_social_login__WEBPACK_IMPORTED_MODULE_13__["FacebookLoginProvider"].PROVIDER_ID,
        provider: new angularx_social_login__WEBPACK_IMPORTED_MODULE_13__["FacebookLoginProvider"]('561602290896109')
    },
    {
        id: angularx_social_login__WEBPACK_IMPORTED_MODULE_13__["LinkedInLoginProvider"].PROVIDER_ID,
        provider: new angularx_social_login__WEBPACK_IMPORTED_MODULE_13__["LinkedInLoginProvider"]('78iqy5cu2e1fgr')
    }
]);
function provideConfig() {
    return config;
}
let AppModule = class AppModule {
};
AppModule = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_2__["NgModule"])({
        declarations: [
            _app_component__WEBPACK_IMPORTED_MODULE_5__["AppComponent"],
        ],
        imports: [
            _angular_platform_browser__WEBPACK_IMPORTED_MODULE_1__["BrowserModule"],
            _angular_common_http__WEBPACK_IMPORTED_MODULE_3__["HttpClientModule"],
            _app_routing_module__WEBPACK_IMPORTED_MODULE_4__["AppRoutingModule"],
            _angular_platform_browser_animations__WEBPACK_IMPORTED_MODULE_6__["BrowserAnimationsModule"],
            _auth_auth_module__WEBPACK_IMPORTED_MODULE_7__["AuthModule"],
            _app_material_module__WEBPACK_IMPORTED_MODULE_8__["AppMaterialModule"],
            _news_news_module__WEBPACK_IMPORTED_MODULE_9__["NewsModule"],
            _users_users_module__WEBPACK_IMPORTED_MODULE_10__["UsersModule"],
            _profile_profile_module__WEBPACK_IMPORTED_MODULE_11__["ProfileModule"],
            angularx_social_login__WEBPACK_IMPORTED_MODULE_13__["SocialLoginModule"],
            _auth_social_social_module__WEBPACK_IMPORTED_MODULE_14__["SocialModule"]
        ],
        entryComponents: [_app_component__WEBPACK_IMPORTED_MODULE_5__["AppComponent"]],
        providers: [
            {
                provide: angularx_social_login__WEBPACK_IMPORTED_MODULE_13__["AuthServiceConfig"],
                useFactory: provideConfig,
                useClass: _auth_token_interceptor__WEBPACK_IMPORTED_MODULE_12__["TokenInterceptor"],
                multi: true
            }
        ],
        bootstrap: [_app_component__WEBPACK_IMPORTED_MODULE_5__["AppComponent"]]
    })
], AppModule);



/***/ }),

/***/ "./src/app/auth/auth.module.ts":
/*!*************************************!*\
  !*** ./src/app/auth/auth.module.ts ***!
  \*************************************/
/*! exports provided: AuthModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "AuthModule", function() { return AuthModule; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_platform_browser__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/platform-browser */ "./node_modules/@angular/platform-browser/fesm2015/platform-browser.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm2015/core.js");
/* harmony import */ var _angular_common_http__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/common/http */ "./node_modules/@angular/common/fesm2015/http.js");
/* harmony import */ var _registration_registration_module__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ./registration/registration.module */ "./src/app/auth/registration/registration.module.ts");
/* harmony import */ var _auth_service__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ./auth.service */ "./src/app/auth/auth.service.ts");
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! @angular/forms */ "./node_modules/@angular/forms/fesm2015/forms.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm2015/router.js");
/* harmony import */ var _login_login_module__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(/*! ./login/login.module */ "./src/app/auth/login/login.module.ts");
/* harmony import */ var _logout_component__WEBPACK_IMPORTED_MODULE_9__ = __webpack_require__(/*! ./logout.component */ "./src/app/auth/logout.component.ts");










let AuthModule = class AuthModule {
};
AuthModule = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_2__["NgModule"])({
        imports: [
            _angular_platform_browser__WEBPACK_IMPORTED_MODULE_1__["BrowserModule"],
            _angular_common_http__WEBPACK_IMPORTED_MODULE_3__["HttpClientModule"],
            _angular_router__WEBPACK_IMPORTED_MODULE_7__["RouterModule"],
            _registration_registration_module__WEBPACK_IMPORTED_MODULE_4__["RegistrationModule"],
            _login_login_module__WEBPACK_IMPORTED_MODULE_8__["LoginModule"],
            _angular_forms__WEBPACK_IMPORTED_MODULE_6__["FormsModule"],
        ],
        declarations: [_logout_component__WEBPACK_IMPORTED_MODULE_9__["LogoutComponent"]],
        exports: [_logout_component__WEBPACK_IMPORTED_MODULE_9__["LogoutComponent"]],
        entryComponents: [],
        providers: [_auth_service__WEBPACK_IMPORTED_MODULE_5__["AuthService"]],
    })
], AuthModule);



/***/ }),

/***/ "./src/app/auth/auth.routing.ts":
/*!**************************************!*\
  !*** ./src/app/auth/auth.routing.ts ***!
  \**************************************/
/*! exports provided: authRoutes */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "authRoutes", function() { return authRoutes; });
/* harmony import */ var _login_login_component__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./login/login.component */ "./src/app/auth/login/login.component.ts");
/* harmony import */ var _registration_registration_component__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./registration/registration.component */ "./src/app/auth/registration/registration.component.ts");


const authRoutes = [
    { path: 'auth',
        children: [
            { path: 'login', component: _login_login_component__WEBPACK_IMPORTED_MODULE_0__["LoginComponent"] },
            { path: 'registration', component: _registration_registration_component__WEBPACK_IMPORTED_MODULE_1__["RegistrationComponent"] },
        ]
    }
];


/***/ }),

/***/ "./src/app/auth/auth.service.ts":
/*!**************************************!*\
  !*** ./src/app/auth/auth.service.ts ***!
  \**************************************/
/*! exports provided: AuthService */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "AuthService", function() { return AuthService; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm2015/core.js");
/* harmony import */ var _angular_common_http__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/common/http */ "./node_modules/@angular/common/fesm2015/http.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm2015/router.js");




let AuthService = class AuthService {
    constructor(http, router) {
        this.http = http;
        this.cachedRequests = [];
        this.baseurl = 'http://localhost:8002';
        // сообщения об ошибках авторизации
        this.errors = [];
        this.router = router;
        this.httpOptions = {
            headers: new _angular_common_http__WEBPACK_IMPORTED_MODULE_2__["HttpHeaders"]({ 'Content-Type': 'application/json' })
        };
    }
    collectFailedRequest(request) {
        this.cachedRequests.push(request);
    }
    retryFailedRequests() {
        // retry the requests. this method can
        // be called after the token is refreshed
    }
    registerNewUser(userData) {
        return this.http.post(this.baseurl + '/users/', userData);
    }
    // используем http.post() для получения токена
    login(user) {
        this.http.post(this.baseurl + '/api-token-auth/', JSON.stringify(user), this.httpOptions).subscribe(data => {
            this.updateData(data['token']);
            // alert('Welcome! ' + this.username);
            localStorage.setItem('authToken', data['token']);
            this.router.navigate(['../news']);
        }, err => {
            this.errors = err['error'];
        });
    }
    getToken() {
        return localStorage.getItem('token');
    }
    // обновление JWT токена
    refreshToken() {
        this.http.post(this.baseurl + '/api-token-refresh/', JSON.stringify({ token: this.token }), this.httpOptions).subscribe(data => {
            this.updateData(data['token']);
        }, err => {
            this.errors = err['error'];
        });
    }
    logout() {
        localStorage.removeItem('authToken');
        this.router.navigate(['/auth/login']);
    }
    updateData(token) {
        this.token = token;
        this.errors = [];
        // декодирование токена для получения логина и времени жизни токена
        const token_parts = this.token.split(/\./);
        const token_decoded = JSON.parse(window.atob(token_parts[1]));
        this.token_expires = new Date(token_decoded.exp * 1000);
        this.username = token_decoded.username;
    }
    jwtCheck() {
        try {
            const currentToken = localStorage.getItem('authToken');
            // console.log('currentToken', currentToken);
            if (currentToken) {
                this.router.navigate(['../news']);
                return;
            }
            this.router.navigate(['../auth/login']);
        }
        catch (e) {
            console.log('e', e);
        }
    }
};
AuthService.ctorParameters = () => [
    { type: _angular_common_http__WEBPACK_IMPORTED_MODULE_2__["HttpClient"] },
    { type: _angular_router__WEBPACK_IMPORTED_MODULE_3__["Router"] }
];
AuthService = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Injectable"])()
], AuthService);



/***/ }),

/***/ "./src/app/auth/login/login-material.module.ts":
/*!*****************************************************!*\
  !*** ./src/app/auth/login/login-material.module.ts ***!
  \*****************************************************/
/*! exports provided: LoginMaterialModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "LoginMaterialModule", function() { return LoginMaterialModule; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm2015/core.js");
/* harmony import */ var _angular_material__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/material */ "./node_modules/@angular/material/esm2015/material.js");



let LoginMaterialModule = class LoginMaterialModule {
};
LoginMaterialModule = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
        declarations: [],
        imports: [
            _angular_material__WEBPACK_IMPORTED_MODULE_2__["MatButtonModule"],
            _angular_material__WEBPACK_IMPORTED_MODULE_2__["MatChipsModule"],
            _angular_material__WEBPACK_IMPORTED_MODULE_2__["MatCardModule"],
            _angular_material__WEBPACK_IMPORTED_MODULE_2__["MatIconModule"],
            _angular_material__WEBPACK_IMPORTED_MODULE_2__["MatInputModule"],
            _angular_material__WEBPACK_IMPORTED_MODULE_2__["MatToolbarModule"],
            _angular_material__WEBPACK_IMPORTED_MODULE_2__["MatSelectModule"],
            _angular_material__WEBPACK_IMPORTED_MODULE_2__["MatAutocompleteModule"],
            _angular_material__WEBPACK_IMPORTED_MODULE_2__["MatInputModule"],
            _angular_material__WEBPACK_IMPORTED_MODULE_2__["MatCheckboxModule"],
            _angular_material__WEBPACK_IMPORTED_MODULE_2__["MatDialogModule"],
            _angular_material__WEBPACK_IMPORTED_MODULE_2__["MatFormFieldModule"],
        ],
        exports: [
            _angular_material__WEBPACK_IMPORTED_MODULE_2__["MatButtonModule"],
            _angular_material__WEBPACK_IMPORTED_MODULE_2__["MatChipsModule"],
            _angular_material__WEBPACK_IMPORTED_MODULE_2__["MatCardModule"],
            _angular_material__WEBPACK_IMPORTED_MODULE_2__["MatIconModule"],
            _angular_material__WEBPACK_IMPORTED_MODULE_2__["MatInputModule"],
            _angular_material__WEBPACK_IMPORTED_MODULE_2__["MatToolbarModule"],
            _angular_material__WEBPACK_IMPORTED_MODULE_2__["MatSelectModule"],
            _angular_material__WEBPACK_IMPORTED_MODULE_2__["MatAutocompleteModule"],
            _angular_material__WEBPACK_IMPORTED_MODULE_2__["MatInputModule"],
            _angular_material__WEBPACK_IMPORTED_MODULE_2__["MatCheckboxModule"],
            _angular_material__WEBPACK_IMPORTED_MODULE_2__["MatDialogModule"],
            _angular_material__WEBPACK_IMPORTED_MODULE_2__["MatFormFieldModule"],
        ],
    })
], LoginMaterialModule);



/***/ }),

/***/ "./src/app/auth/login/login.component.css":
/*!************************************************!*\
  !*** ./src/app/auth/login/login.component.css ***!
  \************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "  span {\n    color: red;\n  }\n  \n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbInNyYy9hcHAvYXV0aC9sb2dpbi9sb2dpbi5jb21wb25lbnQuY3NzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiJFQUFFO0lBQ0UsVUFBVTtFQUNaIiwiZmlsZSI6InNyYy9hcHAvYXV0aC9sb2dpbi9sb2dpbi5jb21wb25lbnQuY3NzIiwic291cmNlc0NvbnRlbnQiOlsiICBzcGFuIHtcbiAgICBjb2xvcjogcmVkO1xuICB9XG4gICJdfQ== */"

/***/ }),

/***/ "./src/app/auth/login/login.component.ts":
/*!***********************************************!*\
  !*** ./src/app/auth/login/login.component.ts ***!
  \***********************************************/
/*! exports provided: LoginComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "LoginComponent", function() { return LoginComponent; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm2015/core.js");
/* harmony import */ var _auth_service__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../auth.service */ "./src/app/auth/auth.service.ts");



let LoginComponent = class LoginComponent {
    constructor(authService) {
        this.authService = authService;
    }
    ngOnInit() {
        this.user = {
            username: '',
            password: ''
        };
    }
    login() {
        this.authService.login({ 'username': this.user.username, 'password': this.user.password });
    }
    refreshToken() {
        this.authService.refreshToken();
    }
    logout() {
        this.authService.logout();
    }
    keytab(event) {
        let element = event.srcElement.nextElementSibling; // get the sibling element
        if (element == null) // check if its null
            return;
        else
            element.focus(); // focus if not null
    }
};
LoginComponent.ctorParameters = () => [
    { type: _auth_service__WEBPACK_IMPORTED_MODULE_2__["AuthService"] }
];
LoginComponent = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
        selector: 'app-login',
        template: __webpack_require__(/*! raw-loader!./login.component.html */ "./node_modules/raw-loader/index.js!./src/app/auth/login/login.component.html"),
        styles: [__webpack_require__(/*! ./login.component.css */ "./src/app/auth/login/login.component.css")]
    })
], LoginComponent);



/***/ }),

/***/ "./src/app/auth/login/login.module.ts":
/*!********************************************!*\
  !*** ./src/app/auth/login/login.module.ts ***!
  \********************************************/
/*! exports provided: LoginModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "LoginModule", function() { return LoginModule; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm2015/core.js");
/* harmony import */ var _angular_common__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/common */ "./node_modules/@angular/common/fesm2015/common.js");
/* harmony import */ var _login_component__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ./login.component */ "./src/app/auth/login/login.component.ts");
/* harmony import */ var _login_material_module__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ./login-material.module */ "./src/app/auth/login/login-material.module.ts");
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! @angular/forms */ "./node_modules/@angular/forms/fesm2015/forms.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm2015/router.js");
/* harmony import */ var _social_social_module__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! ../social/social.module */ "./src/app/auth/social/social.module.ts");








let LoginModule = class LoginModule {
};
LoginModule = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
        imports: [
            _angular_common__WEBPACK_IMPORTED_MODULE_2__["CommonModule"],
            _login_material_module__WEBPACK_IMPORTED_MODULE_4__["LoginMaterialModule"],
            _angular_forms__WEBPACK_IMPORTED_MODULE_5__["FormsModule"],
            _angular_router__WEBPACK_IMPORTED_MODULE_6__["RouterModule"],
            _social_social_module__WEBPACK_IMPORTED_MODULE_7__["SocialModule"]
        ],
        declarations: [_login_component__WEBPACK_IMPORTED_MODULE_3__["LoginComponent"]],
        entryComponents: [_login_component__WEBPACK_IMPORTED_MODULE_3__["LoginComponent"]]
    })
], LoginModule);



/***/ }),

/***/ "./src/app/auth/logout.component.ts":
/*!******************************************!*\
  !*** ./src/app/auth/logout.component.ts ***!
  \******************************************/
/*! exports provided: LogoutComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "LogoutComponent", function() { return LogoutComponent; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm2015/core.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm2015/router.js");
/* harmony import */ var _auth_service__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ./auth.service */ "./src/app/auth/auth.service.ts");




let LogoutComponent = class LogoutComponent {
    constructor(authService, router) {
        this.authService = authService;
        this.router = router;
    }
    logout() {
        this.authService.logout();
    }
};
LogoutComponent.ctorParameters = () => [
    { type: _auth_service__WEBPACK_IMPORTED_MODULE_3__["AuthService"] },
    { type: _angular_router__WEBPACK_IMPORTED_MODULE_2__["Router"] }
];
LogoutComponent = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
        selector: 'app-logout',
        template: __webpack_require__(/*! raw-loader!./logout.component.html */ "./node_modules/raw-loader/index.js!./src/app/auth/logout.component.html"),
    })
], LogoutComponent);



/***/ }),

/***/ "./src/app/auth/registration/registration-material.module.ts":
/*!*******************************************************************!*\
  !*** ./src/app/auth/registration/registration-material.module.ts ***!
  \*******************************************************************/
/*! exports provided: RegistrationMaterialModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "RegistrationMaterialModule", function() { return RegistrationMaterialModule; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm2015/core.js");
/* harmony import */ var _angular_material__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/material */ "./node_modules/@angular/material/esm2015/material.js");



let RegistrationMaterialModule = class RegistrationMaterialModule {
};
RegistrationMaterialModule = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
        declarations: [],
        imports: [
            _angular_material__WEBPACK_IMPORTED_MODULE_2__["MatButtonModule"],
            _angular_material__WEBPACK_IMPORTED_MODULE_2__["MatChipsModule"],
            _angular_material__WEBPACK_IMPORTED_MODULE_2__["MatCardModule"],
            _angular_material__WEBPACK_IMPORTED_MODULE_2__["MatIconModule"],
            _angular_material__WEBPACK_IMPORTED_MODULE_2__["MatInputModule"],
            _angular_material__WEBPACK_IMPORTED_MODULE_2__["MatToolbarModule"],
            _angular_material__WEBPACK_IMPORTED_MODULE_2__["MatSelectModule"],
            _angular_material__WEBPACK_IMPORTED_MODULE_2__["MatAutocompleteModule"],
            _angular_material__WEBPACK_IMPORTED_MODULE_2__["MatInputModule"],
            _angular_material__WEBPACK_IMPORTED_MODULE_2__["MatCheckboxModule"],
            _angular_material__WEBPACK_IMPORTED_MODULE_2__["MatDialogModule"],
            _angular_material__WEBPACK_IMPORTED_MODULE_2__["MatFormFieldModule"],
        ],
        exports: [
            _angular_material__WEBPACK_IMPORTED_MODULE_2__["MatButtonModule"],
            _angular_material__WEBPACK_IMPORTED_MODULE_2__["MatChipsModule"],
            _angular_material__WEBPACK_IMPORTED_MODULE_2__["MatCardModule"],
            _angular_material__WEBPACK_IMPORTED_MODULE_2__["MatIconModule"],
            _angular_material__WEBPACK_IMPORTED_MODULE_2__["MatInputModule"],
            _angular_material__WEBPACK_IMPORTED_MODULE_2__["MatToolbarModule"],
            _angular_material__WEBPACK_IMPORTED_MODULE_2__["MatSelectModule"],
            _angular_material__WEBPACK_IMPORTED_MODULE_2__["MatAutocompleteModule"],
            _angular_material__WEBPACK_IMPORTED_MODULE_2__["MatInputModule"],
            _angular_material__WEBPACK_IMPORTED_MODULE_2__["MatCheckboxModule"],
            _angular_material__WEBPACK_IMPORTED_MODULE_2__["MatDialogModule"],
            _angular_material__WEBPACK_IMPORTED_MODULE_2__["MatFormFieldModule"],
        ],
    })
], RegistrationMaterialModule);



/***/ }),

/***/ "./src/app/auth/registration/registration.component.css":
/*!**************************************************************!*\
  !*** ./src/app/auth/registration/registration.component.css ***!
  \**************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "span {\n  color: red;\n}\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbInNyYy9hcHAvYXV0aC9yZWdpc3RyYXRpb24vcmVnaXN0cmF0aW9uLmNvbXBvbmVudC5jc3MiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IkFBQUE7RUFDRSxVQUFVO0FBQ1oiLCJmaWxlIjoic3JjL2FwcC9hdXRoL3JlZ2lzdHJhdGlvbi9yZWdpc3RyYXRpb24uY29tcG9uZW50LmNzcyIsInNvdXJjZXNDb250ZW50IjpbInNwYW4ge1xuICBjb2xvcjogcmVkO1xufSJdfQ== */"

/***/ }),

/***/ "./src/app/auth/registration/registration.component.ts":
/*!*************************************************************!*\
  !*** ./src/app/auth/registration/registration.component.ts ***!
  \*************************************************************/
/*! exports provided: RegistrationComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "RegistrationComponent", function() { return RegistrationComponent; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm2015/core.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm2015/router.js");
/* harmony import */ var _auth_service__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ../auth.service */ "./src/app/auth/auth.service.ts");




let RegistrationComponent = class RegistrationComponent {
    constructor(authService, router) {
        this.authService = authService;
        this.router = router;
        // сообщения об ошибках авторизации
        this.errors = [];
    }
    ngOnInit() {
        this.register = {
            first_name: '',
            last_name: '',
            username: '',
            password: '',
            email: '',
        };
    }
    registerUser() {
        this.authService.registerNewUser(this.register).subscribe(response => {
            alert('Registration successful! ' + this.register.username + ' has been created');
            this.router.navigate(['/auth/login']);
        }, error => {
            this.errors = error['error'];
            console.log('error', this.errors);
        });
    }
};
RegistrationComponent.ctorParameters = () => [
    { type: _auth_service__WEBPACK_IMPORTED_MODULE_3__["AuthService"] },
    { type: _angular_router__WEBPACK_IMPORTED_MODULE_2__["Router"] }
];
RegistrationComponent = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
        selector: 'app-registration',
        template: __webpack_require__(/*! raw-loader!./registration.component.html */ "./node_modules/raw-loader/index.js!./src/app/auth/registration/registration.component.html"),
        styles: [__webpack_require__(/*! ./registration.component.css */ "./src/app/auth/registration/registration.component.css")]
    })
], RegistrationComponent);



/***/ }),

/***/ "./src/app/auth/registration/registration.module.ts":
/*!**********************************************************!*\
  !*** ./src/app/auth/registration/registration.module.ts ***!
  \**********************************************************/
/*! exports provided: RegistrationModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "RegistrationModule", function() { return RegistrationModule; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm2015/core.js");
/* harmony import */ var _registration_component__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ./registration.component */ "./src/app/auth/registration/registration.component.ts");
/* harmony import */ var _angular_common__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/common */ "./node_modules/@angular/common/fesm2015/common.js");
/* harmony import */ var _registration_material_module__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ./registration-material.module */ "./src/app/auth/registration/registration-material.module.ts");
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! @angular/forms */ "./node_modules/@angular/forms/fesm2015/forms.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm2015/router.js");







let RegistrationModule = class RegistrationModule {
};
RegistrationModule = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
        imports: [_angular_common__WEBPACK_IMPORTED_MODULE_3__["CommonModule"], _registration_material_module__WEBPACK_IMPORTED_MODULE_4__["RegistrationMaterialModule"], _angular_forms__WEBPACK_IMPORTED_MODULE_5__["FormsModule"], _angular_router__WEBPACK_IMPORTED_MODULE_6__["RouterModule"]],
        declarations: [_registration_component__WEBPACK_IMPORTED_MODULE_2__["RegistrationComponent"]],
        entryComponents: [_registration_component__WEBPACK_IMPORTED_MODULE_2__["RegistrationComponent"]]
    })
], RegistrationModule);



/***/ }),

/***/ "./src/app/auth/social/social.component.css":
/*!**************************************************!*\
  !*** ./src/app/auth/social/social.component.css ***!
  \**************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IiIsImZpbGUiOiJzcmMvYXBwL2F1dGgvc29jaWFsL3NvY2lhbC5jb21wb25lbnQuY3NzIn0= */"

/***/ }),

/***/ "./src/app/auth/social/social.component.ts":
/*!*************************************************!*\
  !*** ./src/app/auth/social/social.component.ts ***!
  \*************************************************/
/*! exports provided: SocialComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "SocialComponent", function() { return SocialComponent; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm2015/core.js");
/* harmony import */ var angularx_social_login__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! angularx-social-login */ "./node_modules/angularx-social-login/angularx-social-login.js");




let SocialComponent = class SocialComponent {
    constructor(authService) {
        this.authService = authService;
    }
    ngOnInit() {
        this.authService.authState.subscribe((user) => {
            this.user = user;
        });
    }
    signInWithGoogle() {
        this.authService.signIn(angularx_social_login__WEBPACK_IMPORTED_MODULE_2__["GoogleLoginProvider"].PROVIDER_ID);
    }
    signInWithFB() {
        this.authService.signIn(angularx_social_login__WEBPACK_IMPORTED_MODULE_2__["FacebookLoginProvider"].PROVIDER_ID);
    }
    signInWithLinkedIn() {
        this.authService.signIn(angularx_social_login__WEBPACK_IMPORTED_MODULE_2__["LinkedInLoginProvider"].PROVIDER_ID);
    }
    signOut() {
        this.authService.signOut();
    }
};
SocialComponent.ctorParameters = () => [
    { type: angularx_social_login__WEBPACK_IMPORTED_MODULE_2__["AuthService"] }
];
SocialComponent = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
        selector: 'app-social',
        template: __webpack_require__(/*! raw-loader!./social.component.html */ "./node_modules/raw-loader/index.js!./src/app/auth/social/social.component.html"),
        styles: [__webpack_require__(/*! ./social.component.css */ "./src/app/auth/social/social.component.css")]
    })
], SocialComponent);



/***/ }),

/***/ "./src/app/auth/social/social.module.ts":
/*!**********************************************!*\
  !*** ./src/app/auth/social/social.module.ts ***!
  \**********************************************/
/*! exports provided: SocialModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "SocialModule", function() { return SocialModule; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm2015/core.js");
/* harmony import */ var _angular_common__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/common */ "./node_modules/@angular/common/fesm2015/common.js");
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/forms */ "./node_modules/@angular/forms/fesm2015/forms.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm2015/router.js");
/* harmony import */ var src_app_app_material_module__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! src/app/app-material.module */ "./src/app/app-material.module.ts");
/* harmony import */ var _social_component__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ./social.component */ "./src/app/auth/social/social.component.ts");







let SocialModule = class SocialModule {
};
SocialModule = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
        imports: [_angular_common__WEBPACK_IMPORTED_MODULE_2__["CommonModule"], src_app_app_material_module__WEBPACK_IMPORTED_MODULE_5__["AppMaterialModule"], _angular_forms__WEBPACK_IMPORTED_MODULE_3__["FormsModule"], _angular_router__WEBPACK_IMPORTED_MODULE_4__["RouterModule"]],
        declarations: [_social_component__WEBPACK_IMPORTED_MODULE_6__["SocialComponent"]],
        entryComponents: [_social_component__WEBPACK_IMPORTED_MODULE_6__["SocialComponent"]],
        exports: [_social_component__WEBPACK_IMPORTED_MODULE_6__["SocialComponent"]],
    })
], SocialModule);



/***/ }),

/***/ "./src/app/auth/token.interceptor.ts":
/*!*******************************************!*\
  !*** ./src/app/auth/token.interceptor.ts ***!
  \*******************************************/
/*! exports provided: TokenInterceptor */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "TokenInterceptor", function() { return TokenInterceptor; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm2015/core.js");
/* harmony import */ var _auth_service__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ./auth.service */ "./src/app/auth/auth.service.ts");



let TokenInterceptor = class TokenInterceptor {
    constructor(auth) {
        this.auth = auth;
    }
    intercept(request, next) {
        request = request.clone({
            setHeaders: {
                Authorization: `${this.auth.getToken()}`,
            }
        });
        return next.handle(request);
    }
};
TokenInterceptor.ctorParameters = () => [
    { type: _auth_service__WEBPACK_IMPORTED_MODULE_2__["AuthService"] }
];
TokenInterceptor = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Injectable"])()
], TokenInterceptor);



/***/ }),

/***/ "./src/app/fileupload/fileupload.component.css":
/*!*****************************************************!*\
  !*** ./src/app/fileupload/fileupload.component.css ***!
  \*****************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IiIsImZpbGUiOiJzcmMvYXBwL2ZpbGV1cGxvYWQvZmlsZXVwbG9hZC5jb21wb25lbnQuY3NzIn0= */"

/***/ }),

/***/ "./src/app/fileupload/fileupload.component.ts":
/*!****************************************************!*\
  !*** ./src/app/fileupload/fileupload.component.ts ***!
  \****************************************************/
/*! exports provided: FileuploadComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "FileuploadComponent", function() { return FileuploadComponent; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm2015/core.js");
/* harmony import */ var _angular_common_http__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/common/http */ "./node_modules/@angular/common/fesm2015/http.js");



let FileuploadComponent = class FileuploadComponent {
    constructor(http) {
        this.http = http;
        this.baseurl = 'http://localhost:8002';
        this.fileData = null;
        this.url = '';
        this.urlEvent = new _angular_core__WEBPACK_IMPORTED_MODULE_1__["EventEmitter"]();
    }
    ngOnInit() {
    }
    handleFileInput(event) {
        this.fileData = event.target.files[0];
        var reader = new FileReader();
        reader.readAsDataURL(event.target.files[0]);
        reader.onload = (event) => {
            this.url = event.target['result'];
        };
        let formData = new FormData();
        formData.append('file', this.fileData, this.fileData.name);
        // console.log('formData', formData);
        this.http.post(this.baseurl + '/file/upload/', formData, {
            reportProgress: true,
            observe: 'events',
        })
            .subscribe(events => {
            if (events.type == _angular_common_http__WEBPACK_IMPORTED_MODULE_2__["HttpEventType"].UploadProgress) {
                console.log('Upload progress: ', Math.round(events.loaded / events.total * 100) + '%');
            }
            else if (events.type === _angular_common_http__WEBPACK_IMPORTED_MODULE_2__["HttpEventType"].Response) {
                console.log(events);
                const url = ('' + events.url + events.body['file']).replace("/file/upload/", "");
                console.log('avatar_pre:', url);
                this.urlEvent.emit(url);
                // console.log();
            }
        });
    }
};
FileuploadComponent.ctorParameters = () => [
    { type: _angular_common_http__WEBPACK_IMPORTED_MODULE_2__["HttpClient"] }
];
tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Output"])()
], FileuploadComponent.prototype, "urlEvent", void 0);
FileuploadComponent = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
        selector: 'app-fileupload',
        template: __webpack_require__(/*! raw-loader!./fileupload.component.html */ "./node_modules/raw-loader/index.js!./src/app/fileupload/fileupload.component.html"),
        styles: [__webpack_require__(/*! ./fileupload.component.css */ "./src/app/fileupload/fileupload.component.css")]
    })
], FileuploadComponent);



/***/ }),

/***/ "./src/app/fileupload/fileupload.module.ts":
/*!*************************************************!*\
  !*** ./src/app/fileupload/fileupload.module.ts ***!
  \*************************************************/
/*! exports provided: FileUploadModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "FileUploadModule", function() { return FileUploadModule; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm2015/core.js");
/* harmony import */ var _angular_common__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/common */ "./node_modules/@angular/common/fesm2015/common.js");
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/forms */ "./node_modules/@angular/forms/fesm2015/forms.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm2015/router.js");
/* harmony import */ var _fileupload_component__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ./fileupload.component */ "./src/app/fileupload/fileupload.component.ts");
/* harmony import */ var _profile_profile_material_module__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ../profile/profile-material.module */ "./src/app/profile/profile-material.module.ts");







let FileUploadModule = class FileUploadModule {
};
FileUploadModule = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
        imports: [
            _angular_common__WEBPACK_IMPORTED_MODULE_2__["CommonModule"],
            _profile_profile_material_module__WEBPACK_IMPORTED_MODULE_6__["ProfileMaterialModule"],
            _angular_forms__WEBPACK_IMPORTED_MODULE_3__["FormsModule"],
            _angular_router__WEBPACK_IMPORTED_MODULE_4__["RouterModule"],
        ],
        declarations: [_fileupload_component__WEBPACK_IMPORTED_MODULE_5__["FileuploadComponent"]],
        exports: [_fileupload_component__WEBPACK_IMPORTED_MODULE_5__["FileuploadComponent"]],
        entryComponents: [_fileupload_component__WEBPACK_IMPORTED_MODULE_5__["FileuploadComponent"]],
        providers: []
    })
], FileUploadModule);



/***/ }),

/***/ "./src/app/filter.pipe.ts":
/*!********************************!*\
  !*** ./src/app/filter.pipe.ts ***!
  \********************************/
/*! exports provided: FilterPipe */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "FilterPipe", function() { return FilterPipe; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm2015/core.js");


let FilterPipe = class FilterPipe {
    constructor() {
        this.newsEvent = new _angular_core__WEBPACK_IMPORTED_MODULE_1__["EventEmitter"]();
    }
    // public items: any[];
    transform(items, searchText, field) {
        if (!items) {
            return [];
        }
        if (!searchText) {
            return items;
        }
        searchText = searchText.toLocaleLowerCase();
        if (field === "all-0" || !field) {
            items = items.filter(item => {
                return JSON.stringify(item).toLocaleLowerCase().includes(searchText);
            });
            return items;
        }
        else if (field === "tags-1") {
            items = items.filter(item => {
                if (item.tags != null) {
                    return item.tags.toString().toLocaleLowerCase().includes(searchText);
                }
            });
            console.log('PIPE', items);
            this.sendNews(items);
            return items;
        }
        else if (field === "authors-2") {
            items = items.filter(item => {
                return item.author.includes(searchText);
            });
            return items;
        }
    }
    sendNews(items) {
        console.log("SEND NEWS");
        this.newsEvent.emit(items);
    }
};
tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Output"])()
], FilterPipe.prototype, "newsEvent", void 0);
FilterPipe = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Pipe"])({
        name: 'filter'
    })
], FilterPipe);



/***/ }),

/***/ "./src/app/news/news-material.module.ts":
/*!**********************************************!*\
  !*** ./src/app/news/news-material.module.ts ***!
  \**********************************************/
/*! exports provided: NewsMaterialModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "NewsMaterialModule", function() { return NewsMaterialModule; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm2015/core.js");
/* harmony import */ var _angular_material__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/material */ "./node_modules/@angular/material/esm2015/material.js");



let NewsMaterialModule = class NewsMaterialModule {
};
NewsMaterialModule = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
        declarations: [],
        imports: [
            _angular_material__WEBPACK_IMPORTED_MODULE_2__["MatButtonModule"],
            _angular_material__WEBPACK_IMPORTED_MODULE_2__["MatChipsModule"],
            _angular_material__WEBPACK_IMPORTED_MODULE_2__["MatCardModule"],
            _angular_material__WEBPACK_IMPORTED_MODULE_2__["MatIconModule"],
            _angular_material__WEBPACK_IMPORTED_MODULE_2__["MatInputModule"],
            _angular_material__WEBPACK_IMPORTED_MODULE_2__["MatToolbarModule"],
            _angular_material__WEBPACK_IMPORTED_MODULE_2__["MatSelectModule"],
            _angular_material__WEBPACK_IMPORTED_MODULE_2__["MatAutocompleteModule"],
            _angular_material__WEBPACK_IMPORTED_MODULE_2__["MatInputModule"],
            _angular_material__WEBPACK_IMPORTED_MODULE_2__["MatCheckboxModule"],
            _angular_material__WEBPACK_IMPORTED_MODULE_2__["MatDialogModule"],
            _angular_material__WEBPACK_IMPORTED_MODULE_2__["MatFormFieldModule"],
        ],
        exports: [
            _angular_material__WEBPACK_IMPORTED_MODULE_2__["MatButtonModule"],
            _angular_material__WEBPACK_IMPORTED_MODULE_2__["MatChipsModule"],
            _angular_material__WEBPACK_IMPORTED_MODULE_2__["MatCardModule"],
            _angular_material__WEBPACK_IMPORTED_MODULE_2__["MatIconModule"],
            _angular_material__WEBPACK_IMPORTED_MODULE_2__["MatInputModule"],
            _angular_material__WEBPACK_IMPORTED_MODULE_2__["MatToolbarModule"],
            _angular_material__WEBPACK_IMPORTED_MODULE_2__["MatSelectModule"],
            _angular_material__WEBPACK_IMPORTED_MODULE_2__["MatAutocompleteModule"],
            _angular_material__WEBPACK_IMPORTED_MODULE_2__["MatInputModule"],
            _angular_material__WEBPACK_IMPORTED_MODULE_2__["MatCheckboxModule"],
            _angular_material__WEBPACK_IMPORTED_MODULE_2__["MatDialogModule"],
            _angular_material__WEBPACK_IMPORTED_MODULE_2__["MatFormFieldModule"],
        ],
    })
], NewsMaterialModule);



/***/ }),

/***/ "./src/app/news/news.component.css":
/*!*****************************************!*\
  !*** ./src/app/news/news.component.css ***!
  \*****************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "a:focus, a:hover {\n  color: #ffffff;\n  text-decoration: none;\n}\n.pirple:hover {\n  color: #6665fe;\n  text-decoration: none;\n}\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbInNyYy9hcHAvbmV3cy9uZXdzLmNvbXBvbmVudC5jc3MiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IkFBQUE7RUFDRSxjQUFjO0VBQ2QscUJBQXFCO0FBQ3ZCO0FBQ0E7RUFDRSxjQUFjO0VBQ2QscUJBQXFCO0FBQ3ZCIiwiZmlsZSI6InNyYy9hcHAvbmV3cy9uZXdzLmNvbXBvbmVudC5jc3MiLCJzb3VyY2VzQ29udGVudCI6WyJhOmZvY3VzLCBhOmhvdmVyIHtcbiAgY29sb3I6ICNmZmZmZmY7XG4gIHRleHQtZGVjb3JhdGlvbjogbm9uZTtcbn1cbi5waXJwbGU6aG92ZXIge1xuICBjb2xvcjogIzY2NjVmZTtcbiAgdGV4dC1kZWNvcmF0aW9uOiBub25lO1xufSJdfQ== */"

/***/ }),

/***/ "./src/app/news/news.component.ts":
/*!****************************************!*\
  !*** ./src/app/news/news.component.ts ***!
  \****************************************/
/*! exports provided: NewsComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "NewsComponent", function() { return NewsComponent; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm2015/core.js");
/* harmony import */ var _news_service__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ./news.service */ "./src/app/news/news.service.ts");
/* harmony import */ var rxjs_operators__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! rxjs/operators */ "./node_modules/rxjs/_esm2015/operators/index.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm2015/router.js");





let NewsComponent = class NewsComponent {
    constructor(newsService, router) {
        this.newsService = newsService;
        this.router = router;
        this.news = [];
        this.users = [];
        this.userName = [];
        // сообщения об ошибках авторизации
        this.errors = [];
        this.filters = [
            { value: 'all-0', viewValue: 'All' },
            { value: 'tags-1', viewValue: 'Tags' },
            { value: 'authors-2', viewValue: 'Authors' }
        ];
    }
    receiveNews($event) {
        console.log($event);
        this.news = $event;
    }
    ngOnInit() {
        this.token = localStorage.getItem('authToken');
        const token_parts = this.token.split(/\./);
        const token_decoded = JSON.parse(window.atob(token_parts[1]));
        this.userIDToken = token_decoded.user_id;
        //USERS
        this.newsService.getAllUsers()
            .pipe(Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_3__["first"])()).subscribe(response => {
            this.users = response['results'];
            // console.log(this.users);
        });
        //NEWS
        this.newsService.getAllNews()
            .pipe(Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_3__["first"])()).subscribe(response => {
            this.news = response['results'];
            this.news = this.news.map(newsitem => {
                let users = this.users.map(useritem => {
                    return useritem;
                });
                let userNames = users.find(item => {
                    if (item.id === newsitem.author) {
                        newsitem.author_id = item.id;
                        newsitem.author = item.username;
                        // console.log(newsitem);
                        return item.username;
                    }
                    return false;
                });
                return newsitem;
            });
        });
    }
    getUserPage(UserID) {
        if (UserID == undefined) {
            this.router.navigate(['profile/:' + this.userIDToken + '/']);
        }
        else {
            this.router.navigate(['profile/:' + UserID + '/']);
        }
    }
    onChangePage(pageOfItems) {
        // update current page of items
        this.pageOfItems = pageOfItems;
    }
};
NewsComponent.ctorParameters = () => [
    { type: _news_service__WEBPACK_IMPORTED_MODULE_2__["NewsService"] },
    { type: _angular_router__WEBPACK_IMPORTED_MODULE_4__["Router"] }
];
NewsComponent = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
        selector: 'app-news',
        template: __webpack_require__(/*! raw-loader!./news.component.html */ "./node_modules/raw-loader/index.js!./src/app/news/news.component.html"),
        providers: [_news_service__WEBPACK_IMPORTED_MODULE_2__["NewsService"]],
        styles: [__webpack_require__(/*! ./news.component.css */ "./src/app/news/news.component.css")]
    })
], NewsComponent);



/***/ }),

/***/ "./src/app/news/news.module.ts":
/*!*************************************!*\
  !*** ./src/app/news/news.module.ts ***!
  \*************************************/
/*! exports provided: NewsModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "NewsModule", function() { return NewsModule; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm2015/core.js");
/* harmony import */ var _news_component__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ./news.component */ "./src/app/news/news.component.ts");
/* harmony import */ var _angular_common__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/common */ "./node_modules/@angular/common/fesm2015/common.js");
/* harmony import */ var _news_material_module__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ./news-material.module */ "./src/app/news/news-material.module.ts");
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! @angular/forms */ "./node_modules/@angular/forms/fesm2015/forms.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm2015/router.js");
/* harmony import */ var _filter_pipe__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! ../filter.pipe */ "./src/app/filter.pipe.ts");
/* harmony import */ var _pagination_pagination_module__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(/*! ../pagination/pagination.module */ "./src/app/pagination/pagination.module.ts");
/* harmony import */ var _auth_auth_module__WEBPACK_IMPORTED_MODULE_9__ = __webpack_require__(/*! ../auth/auth.module */ "./src/app/auth/auth.module.ts");
/* harmony import */ var _tags_tags_module__WEBPACK_IMPORTED_MODULE_10__ = __webpack_require__(/*! ../tags/tags.module */ "./src/app/tags/tags.module.ts");











let NewsModule = class NewsModule {
};
NewsModule = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
        imports: [
            _angular_common__WEBPACK_IMPORTED_MODULE_3__["CommonModule"],
            _news_material_module__WEBPACK_IMPORTED_MODULE_4__["NewsMaterialModule"],
            _angular_forms__WEBPACK_IMPORTED_MODULE_5__["FormsModule"],
            _angular_router__WEBPACK_IMPORTED_MODULE_6__["RouterModule"],
            _pagination_pagination_module__WEBPACK_IMPORTED_MODULE_8__["PaginationModule"],
            _auth_auth_module__WEBPACK_IMPORTED_MODULE_9__["AuthModule"],
            _tags_tags_module__WEBPACK_IMPORTED_MODULE_10__["TagsModule"]
        ],
        declarations: [_news_component__WEBPACK_IMPORTED_MODULE_2__["NewsComponent"], _filter_pipe__WEBPACK_IMPORTED_MODULE_7__["FilterPipe"]],
        exports: [_filter_pipe__WEBPACK_IMPORTED_MODULE_7__["FilterPipe"]],
        entryComponents: [_news_component__WEBPACK_IMPORTED_MODULE_2__["NewsComponent"]],
        providers: []
    })
], NewsModule);



/***/ }),

/***/ "./src/app/news/news.service.ts":
/*!**************************************!*\
  !*** ./src/app/news/news.service.ts ***!
  \**************************************/
/*! exports provided: NewsService */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "NewsService", function() { return NewsService; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm2015/core.js");
/* harmony import */ var _angular_common_http__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/common/http */ "./node_modules/@angular/common/fesm2015/http.js");



let NewsService = class NewsService {
    constructor(http) {
        this.http = http;
        this.baseurl = 'http://localhost:8002';
    }
    getAllNews() {
        return this.http.get(this.baseurl + '/news/', {
            headers: {
                Authorization: localStorage.getItem('authToken'),
                'Content-Type': 'application/json'
            }
        });
    }
    getAllUsers() {
        return this.http.get(this.baseurl + '/users/', {
            headers: {
                Authorization: localStorage.getItem('authToken'),
                'Content-Type': 'application/json'
            }
        });
    }
};
NewsService.ctorParameters = () => [
    { type: _angular_common_http__WEBPACK_IMPORTED_MODULE_2__["HttpClient"] }
];
NewsService = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Injectable"])({
        providedIn: 'root'
    })
], NewsService);



/***/ }),

/***/ "./src/app/pagination/pagination.module.ts":
/*!*************************************************!*\
  !*** ./src/app/pagination/pagination.module.ts ***!
  \*************************************************/
/*! exports provided: PaginationModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "PaginationModule", function() { return PaginationModule; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm2015/core.js");
/* harmony import */ var _angular_common__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/common */ "./node_modules/@angular/common/fesm2015/common.js");
/* harmony import */ var jw_angular_pagination__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! jw-angular-pagination */ "./node_modules/jw-angular-pagination/lib/jw-pagination.component.js");
/* harmony import */ var jw_angular_pagination__WEBPACK_IMPORTED_MODULE_3___default = /*#__PURE__*/__webpack_require__.n(jw_angular_pagination__WEBPACK_IMPORTED_MODULE_3__);




let PaginationModule = class PaginationModule {
};
PaginationModule = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
        imports: [
            _angular_common__WEBPACK_IMPORTED_MODULE_2__["CommonModule"]
        ],
        declarations: [
            jw_angular_pagination__WEBPACK_IMPORTED_MODULE_3__["JwPaginationComponent"]
        ],
        exports: [
            jw_angular_pagination__WEBPACK_IMPORTED_MODULE_3__["JwPaginationComponent"]
        ]
    })
], PaginationModule);



/***/ }),

/***/ "./src/app/profile/addnews/addnews.component.css":
/*!*******************************************************!*\
  !*** ./src/app/profile/addnews/addnews.component.css ***!
  \*******************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IiIsImZpbGUiOiJzcmMvYXBwL3Byb2ZpbGUvYWRkbmV3cy9hZGRuZXdzLmNvbXBvbmVudC5jc3MifQ== */"

/***/ }),

/***/ "./src/app/profile/addnews/addnews.component.ts":
/*!******************************************************!*\
  !*** ./src/app/profile/addnews/addnews.component.ts ***!
  \******************************************************/
/*! exports provided: AddNewsComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "AddNewsComponent", function() { return AddNewsComponent; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm2015/core.js");
/* harmony import */ var _angular_material__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/material */ "./node_modules/@angular/material/esm2015/material.js");
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/forms */ "./node_modules/@angular/forms/fesm2015/forms.js");
/* harmony import */ var _canceladdnews_canceladdnews_component__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ../canceladdnews/canceladdnews.component */ "./src/app/profile/canceladdnews/canceladdnews.component.ts");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm2015/router.js");
/* harmony import */ var _profile_service__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ../profile.service */ "./src/app/profile/profile.service.ts");







let AddNewsComponent = class AddNewsComponent {
    constructor(fb, dialog, profileService, router) {
        this.fb = fb;
        this.dialog = dialog;
        this.profileService = profileService;
        this.title = `Early adopters`;
        this.desc = `Early adopters jumping on the bandwagon`;
        this.errors = [];
        this.wasFormChanged = false;
        this.router = router;
    }
    receiveTags($event) {
        this.tags = $event;
    }
    receiveImage($event) {
        this.image = $event;
    }
    // @Output () urlEvent = new EventEmitter<string>()
    ngOnInit() {
        console.log('TAGS', this.tags);
        this.author = Number(this.router.url.split(':')[1]);
        this.addCusForm = this.fb.group({
            title: [this.title, [_angular_forms__WEBPACK_IMPORTED_MODULE_3__["Validators"].required, _angular_forms__WEBPACK_IMPORTED_MODULE_3__["Validators"].pattern('[a-zA-Z0-9_.+-]+([a-zA-Z ]+)*')]],
            desc: [this.desc, [_angular_forms__WEBPACK_IMPORTED_MODULE_3__["Validators"].required, _angular_forms__WEBPACK_IMPORTED_MODULE_3__["Validators"].pattern('[a-zA-Z0-9_.+-]+([a-zA-Z ]+)*')]],
            author: [this.author, [_angular_forms__WEBPACK_IMPORTED_MODULE_3__["Validators"].required, _angular_forms__WEBPACK_IMPORTED_MODULE_3__["Validators"].pattern('[a-zA-Z0-9_.+-]+([a-zA-Z ]+)*')]],
            tags: [this.tags],
            image: [this.image],
        });
        console.log('addCusForm', this.addCusForm);
        this.breakpoint = window.innerWidth <= 600 ? 1 : 2; // Breakpoint observer code
    }
    addNews(news) {
        this.addCusForm.value["tags"] = this.tags;
        this.addCusForm.value["image"] = this.image;
        this.newNews = this.addCusForm.value;
        console.log('addCusForm', this.addCusForm.value);
        this.profileService.addNews(this.newNews).subscribe(response => {
            console.log(response);
        }, error => {
            this.errors = error['error'];
            console.log('error', this.errors);
        });
        this.markAsDirty(this.addCusForm);
        this.router.navigate(['profile/:' + this.author + '/']);
        this.dialog.closeAll();
        // this.router.navigate(['../news']);
        // this.urlEvent.emit(url);
        // window.location.reload();
    }
    //additional window before cancel
    openDialog() {
        console.log(this.wasFormChanged);
        if (this.addCusForm.dirty) {
            const dialogRef = this.dialog.open(_canceladdnews_canceladdnews_component__WEBPACK_IMPORTED_MODULE_4__["CancelAddNewsComponent"], {
                width: '340px',
            });
        }
        else {
            this.dialog.closeAll();
        }
    }
    // tslint:disable-next-line:no-any
    onResize(event) {
        this.breakpoint = event.target.innerWidth <= 600 ? 1 : 2;
    }
    markAsDirty(group) {
        group.markAsDirty();
        // tslint:disable-next-line:forin
        for (const i in group.controls) {
            group.controls[i].markAsDirty();
        }
    }
    formChanged() {
        this.wasFormChanged = true;
    }
};
AddNewsComponent.ctorParameters = () => [
    { type: _angular_forms__WEBPACK_IMPORTED_MODULE_3__["FormBuilder"] },
    { type: _angular_material__WEBPACK_IMPORTED_MODULE_2__["MatDialog"] },
    { type: _profile_service__WEBPACK_IMPORTED_MODULE_6__["ProfileService"] },
    { type: _angular_router__WEBPACK_IMPORTED_MODULE_5__["Router"] }
];
AddNewsComponent = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
        selector: 'app-addnews',
        template: __webpack_require__(/*! raw-loader!./addnews.component.html */ "./node_modules/raw-loader/index.js!./src/app/profile/addnews/addnews.component.html"),
        styles: [__webpack_require__(/*! ./addnews.component.css */ "./src/app/profile/addnews/addnews.component.css")]
    })
], AddNewsComponent);



/***/ }),

/***/ "./src/app/profile/addnews/addnews.module.ts":
/*!***************************************************!*\
  !*** ./src/app/profile/addnews/addnews.module.ts ***!
  \***************************************************/
/*! exports provided: AddNewsModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "AddNewsModule", function() { return AddNewsModule; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm2015/core.js");
/* harmony import */ var _angular_common__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/common */ "./node_modules/@angular/common/fesm2015/common.js");
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/forms */ "./node_modules/@angular/forms/fesm2015/forms.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm2015/router.js");
/* harmony import */ var src_app_tags_tags_module__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! src/app/tags/tags.module */ "./src/app/tags/tags.module.ts");
/* harmony import */ var _addnews_component__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ./addnews.component */ "./src/app/profile/addnews/addnews.component.ts");
/* harmony import */ var _profile_material_module__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! ../profile-material.module */ "./src/app/profile/profile-material.module.ts");
/* harmony import */ var src_app_fileupload_fileupload_module__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(/*! src/app/fileupload/fileupload.module */ "./src/app/fileupload/fileupload.module.ts");









let AddNewsModule = class AddNewsModule {
};
AddNewsModule = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
        imports: [
            _angular_common__WEBPACK_IMPORTED_MODULE_2__["CommonModule"],
            _angular_forms__WEBPACK_IMPORTED_MODULE_3__["FormsModule"],
            _angular_router__WEBPACK_IMPORTED_MODULE_4__["RouterModule"],
            src_app_tags_tags_module__WEBPACK_IMPORTED_MODULE_5__["TagsModule"],
            _profile_material_module__WEBPACK_IMPORTED_MODULE_7__["ProfileMaterialModule"],
            _angular_forms__WEBPACK_IMPORTED_MODULE_3__["ReactiveFormsModule"],
            src_app_fileupload_fileupload_module__WEBPACK_IMPORTED_MODULE_8__["FileUploadModule"],
        ],
        declarations: [_addnews_component__WEBPACK_IMPORTED_MODULE_6__["AddNewsComponent"]],
        exports: [],
        entryComponents: [_addnews_component__WEBPACK_IMPORTED_MODULE_6__["AddNewsComponent"]],
        providers: []
    })
], AddNewsModule);



/***/ }),

/***/ "./src/app/profile/canceladdnews/canceladdnews.component.css":
/*!*******************************************************************!*\
  !*** ./src/app/profile/canceladdnews/canceladdnews.component.css ***!
  \*******************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = ".Cancel-btn{\n    margin-right:.5em;\n    margin-top: 1em;\n    height:2.5em;\n    float: right;\n  }\n.Delete-btn {\n    margin-left:.5em;\n    margin-top: 1em;\n    height:2.5em;\n}\n\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbInNyYy9hcHAvcHJvZmlsZS9jYW5jZWxhZGRuZXdzL2NhbmNlbGFkZG5ld3MuY29tcG9uZW50LmNzcyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiQUFBQTtJQUNJLGlCQUFpQjtJQUNqQixlQUFlO0lBQ2YsWUFBWTtJQUNaLFlBQVk7RUFDZDtBQUNGO0lBQ0ksZ0JBQWdCO0lBQ2hCLGVBQWU7SUFDZixZQUFZO0FBQ2hCIiwiZmlsZSI6InNyYy9hcHAvcHJvZmlsZS9jYW5jZWxhZGRuZXdzL2NhbmNlbGFkZG5ld3MuY29tcG9uZW50LmNzcyIsInNvdXJjZXNDb250ZW50IjpbIi5DYW5jZWwtYnRue1xuICAgIG1hcmdpbi1yaWdodDouNWVtO1xuICAgIG1hcmdpbi10b3A6IDFlbTtcbiAgICBoZWlnaHQ6Mi41ZW07XG4gICAgZmxvYXQ6IHJpZ2h0O1xuICB9XG4uRGVsZXRlLWJ0biB7XG4gICAgbWFyZ2luLWxlZnQ6LjVlbTtcbiAgICBtYXJnaW4tdG9wOiAxZW07XG4gICAgaGVpZ2h0OjIuNWVtO1xufVxuIl19 */"

/***/ }),

/***/ "./src/app/profile/canceladdnews/canceladdnews.component.ts":
/*!******************************************************************!*\
  !*** ./src/app/profile/canceladdnews/canceladdnews.component.ts ***!
  \******************************************************************/
/*! exports provided: CancelAddNewsComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "CancelAddNewsComponent", function() { return CancelAddNewsComponent; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm2015/core.js");
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/forms */ "./node_modules/@angular/forms/fesm2015/forms.js");
/* harmony import */ var _angular_material__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/material */ "./node_modules/@angular/material/esm2015/material.js");




// import {AddNewsComponent} from '../addnews/addnews.component';
let CancelAddNewsComponent = class CancelAddNewsComponent {
    constructor(fb, dialog, dialogRef) {
        this.fb = fb;
        this.dialog = dialog;
        this.dialogRef = dialogRef;
    } // Closing dialog window
    cancel() {
        this.dialogRef.close();
    }
    cancelN() {
        this.dialog.closeAll();
    }
};
CancelAddNewsComponent.ctorParameters = () => [
    { type: _angular_forms__WEBPACK_IMPORTED_MODULE_2__["FormBuilder"] },
    { type: _angular_material__WEBPACK_IMPORTED_MODULE_3__["MatDialog"] },
    { type: _angular_material__WEBPACK_IMPORTED_MODULE_3__["MatDialogRef"] }
];
CancelAddNewsComponent = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
        selector: 'app-canceladdnews',
        template: __webpack_require__(/*! raw-loader!./canceladdnews.component.html */ "./node_modules/raw-loader/index.js!./src/app/profile/canceladdnews/canceladdnews.component.html"),
        styles: [__webpack_require__(/*! ./canceladdnews.component.css */ "./src/app/profile/canceladdnews/canceladdnews.component.css")]
    })
], CancelAddNewsComponent);



/***/ }),

/***/ "./src/app/profile/canceladdnews/canceladdnews.module.ts":
/*!***************************************************************!*\
  !*** ./src/app/profile/canceladdnews/canceladdnews.module.ts ***!
  \***************************************************************/
/*! exports provided: CancelAddNewsModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "CancelAddNewsModule", function() { return CancelAddNewsModule; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm2015/core.js");
/* harmony import */ var _angular_common__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/common */ "./node_modules/@angular/common/fesm2015/common.js");
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/forms */ "./node_modules/@angular/forms/fesm2015/forms.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm2015/router.js");
/* harmony import */ var _canceladdnews_component__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ./canceladdnews.component */ "./src/app/profile/canceladdnews/canceladdnews.component.ts");
/* harmony import */ var _profile_material_module__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ../profile-material.module */ "./src/app/profile/profile-material.module.ts");







let CancelAddNewsModule = class CancelAddNewsModule {
};
CancelAddNewsModule = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
        imports: [
            _angular_common__WEBPACK_IMPORTED_MODULE_2__["CommonModule"],
            _angular_forms__WEBPACK_IMPORTED_MODULE_3__["FormsModule"],
            _angular_router__WEBPACK_IMPORTED_MODULE_4__["RouterModule"],
            _profile_material_module__WEBPACK_IMPORTED_MODULE_6__["ProfileMaterialModule"]
        ],
        declarations: [_canceladdnews_component__WEBPACK_IMPORTED_MODULE_5__["CancelAddNewsComponent"]],
        exports: [],
        entryComponents: [_canceladdnews_component__WEBPACK_IMPORTED_MODULE_5__["CancelAddNewsComponent"]],
        providers: []
    })
], CancelAddNewsModule);



/***/ }),

/***/ "./src/app/profile/profile-material.module.ts":
/*!****************************************************!*\
  !*** ./src/app/profile/profile-material.module.ts ***!
  \****************************************************/
/*! exports provided: ProfileMaterialModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "ProfileMaterialModule", function() { return ProfileMaterialModule; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm2015/core.js");
/* harmony import */ var _angular_cdk_a11y__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/cdk/a11y */ "./node_modules/@angular/cdk/esm2015/a11y.js");
/* harmony import */ var _angular_cdk_bidi__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/cdk/bidi */ "./node_modules/@angular/cdk/esm2015/bidi.js");
/* harmony import */ var _angular_cdk_observers__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @angular/cdk/observers */ "./node_modules/@angular/cdk/esm2015/observers.js");
/* harmony import */ var _angular_cdk_overlay__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! @angular/cdk/overlay */ "./node_modules/@angular/cdk/esm2015/overlay.js");
/* harmony import */ var _angular_cdk_platform__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! @angular/cdk/platform */ "./node_modules/@angular/cdk/esm2015/platform.js");
/* harmony import */ var _angular_cdk_portal__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! @angular/cdk/portal */ "./node_modules/@angular/cdk/esm2015/portal.js");
/* harmony import */ var _angular_cdk_scrolling__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(/*! @angular/cdk/scrolling */ "./node_modules/@angular/cdk/esm2015/scrolling.js");
/* harmony import */ var _angular_cdk_stepper__WEBPACK_IMPORTED_MODULE_9__ = __webpack_require__(/*! @angular/cdk/stepper */ "./node_modules/@angular/cdk/esm2015/stepper.js");
/* harmony import */ var _angular_cdk_table__WEBPACK_IMPORTED_MODULE_10__ = __webpack_require__(/*! @angular/cdk/table */ "./node_modules/@angular/cdk/esm2015/table.js");
/* harmony import */ var _angular_material__WEBPACK_IMPORTED_MODULE_11__ = __webpack_require__(/*! @angular/material */ "./node_modules/@angular/material/esm2015/material.js");












let ProfileMaterialModule = class ProfileMaterialModule {
};
ProfileMaterialModule = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
        declarations: [],
        imports: [
            _angular_material__WEBPACK_IMPORTED_MODULE_11__["MatButtonModule"],
            _angular_material__WEBPACK_IMPORTED_MODULE_11__["MatChipsModule"],
            _angular_material__WEBPACK_IMPORTED_MODULE_11__["MatCardModule"],
            _angular_material__WEBPACK_IMPORTED_MODULE_11__["MatIconModule"],
            _angular_material__WEBPACK_IMPORTED_MODULE_11__["MatInputModule"],
            _angular_material__WEBPACK_IMPORTED_MODULE_11__["MatToolbarModule"],
            _angular_material__WEBPACK_IMPORTED_MODULE_11__["MatSelectModule"],
            _angular_material__WEBPACK_IMPORTED_MODULE_11__["MatAutocompleteModule"],
            _angular_material__WEBPACK_IMPORTED_MODULE_11__["MatInputModule"],
            _angular_material__WEBPACK_IMPORTED_MODULE_11__["MatCheckboxModule"],
            _angular_material__WEBPACK_IMPORTED_MODULE_11__["MatDialogModule"],
            _angular_material__WEBPACK_IMPORTED_MODULE_11__["MatFormFieldModule"],
            _angular_material__WEBPACK_IMPORTED_MODULE_11__["MatGridListModule"]
        ],
        exports: [
            // CDK
            _angular_cdk_a11y__WEBPACK_IMPORTED_MODULE_2__["A11yModule"],
            _angular_cdk_bidi__WEBPACK_IMPORTED_MODULE_3__["BidiModule"],
            _angular_cdk_observers__WEBPACK_IMPORTED_MODULE_4__["ObserversModule"],
            _angular_cdk_overlay__WEBPACK_IMPORTED_MODULE_5__["OverlayModule"],
            _angular_cdk_platform__WEBPACK_IMPORTED_MODULE_6__["PlatformModule"],
            _angular_cdk_portal__WEBPACK_IMPORTED_MODULE_7__["PortalModule"],
            _angular_cdk_scrolling__WEBPACK_IMPORTED_MODULE_8__["ScrollDispatchModule"],
            _angular_cdk_stepper__WEBPACK_IMPORTED_MODULE_9__["CdkStepperModule"],
            _angular_cdk_table__WEBPACK_IMPORTED_MODULE_10__["CdkTableModule"],
            // Material
            _angular_material__WEBPACK_IMPORTED_MODULE_11__["MatAutocompleteModule"],
            _angular_material__WEBPACK_IMPORTED_MODULE_11__["MatButtonModule"],
            _angular_material__WEBPACK_IMPORTED_MODULE_11__["MatButtonToggleModule"],
            _angular_material__WEBPACK_IMPORTED_MODULE_11__["MatCardModule"],
            _angular_material__WEBPACK_IMPORTED_MODULE_11__["MatCheckboxModule"],
            _angular_material__WEBPACK_IMPORTED_MODULE_11__["MatChipsModule"],
            _angular_material__WEBPACK_IMPORTED_MODULE_11__["MatDatepickerModule"],
            _angular_material__WEBPACK_IMPORTED_MODULE_11__["MatDialogModule"],
            _angular_material__WEBPACK_IMPORTED_MODULE_11__["MatExpansionModule"],
            _angular_material__WEBPACK_IMPORTED_MODULE_11__["MatGridListModule"],
            _angular_material__WEBPACK_IMPORTED_MODULE_11__["MatIconModule"],
            _angular_material__WEBPACK_IMPORTED_MODULE_11__["MatInputModule"],
            _angular_material__WEBPACK_IMPORTED_MODULE_11__["MatListModule"],
            _angular_material__WEBPACK_IMPORTED_MODULE_11__["MatMenuModule"],
            _angular_material__WEBPACK_IMPORTED_MODULE_11__["MatProgressBarModule"],
            _angular_material__WEBPACK_IMPORTED_MODULE_11__["MatProgressSpinnerModule"],
            _angular_material__WEBPACK_IMPORTED_MODULE_11__["MatRadioModule"],
            _angular_material__WEBPACK_IMPORTED_MODULE_11__["MatRippleModule"],
            _angular_material__WEBPACK_IMPORTED_MODULE_11__["MatSelectModule"],
            _angular_material__WEBPACK_IMPORTED_MODULE_11__["MatSidenavModule"],
            _angular_material__WEBPACK_IMPORTED_MODULE_11__["MatSlideToggleModule"],
            _angular_material__WEBPACK_IMPORTED_MODULE_11__["MatSliderModule"],
            _angular_material__WEBPACK_IMPORTED_MODULE_11__["MatSnackBarModule"],
            _angular_material__WEBPACK_IMPORTED_MODULE_11__["MatStepperModule"],
            _angular_material__WEBPACK_IMPORTED_MODULE_11__["MatTableModule"],
            _angular_material__WEBPACK_IMPORTED_MODULE_11__["MatTabsModule"],
            _angular_material__WEBPACK_IMPORTED_MODULE_11__["MatToolbarModule"],
            _angular_material__WEBPACK_IMPORTED_MODULE_11__["MatTooltipModule"],
            _angular_material__WEBPACK_IMPORTED_MODULE_11__["MatNativeDateModule"],
        ],
    })
], ProfileMaterialModule);



/***/ }),

/***/ "./src/app/profile/profile.component.css":
/*!***********************************************!*\
  !*** ./src/app/profile/profile.component.css ***!
  \***********************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "a:focus, a:hover {\n  color: #ffffff;\n  text-decoration: none;\n}\n/* app-logout {\n\n} */\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbInNyYy9hcHAvcHJvZmlsZS9wcm9maWxlLmNvbXBvbmVudC5jc3MiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IkFBQUE7RUFDRSxjQUFjO0VBQ2QscUJBQXFCO0FBQ3ZCO0FBQ0E7O0dBRUciLCJmaWxlIjoic3JjL2FwcC9wcm9maWxlL3Byb2ZpbGUuY29tcG9uZW50LmNzcyIsInNvdXJjZXNDb250ZW50IjpbImE6Zm9jdXMsIGE6aG92ZXIge1xuICBjb2xvcjogI2ZmZmZmZjtcbiAgdGV4dC1kZWNvcmF0aW9uOiBub25lO1xufVxuLyogYXBwLWxvZ291dCB7XG5cbn0gKi8iXX0= */"

/***/ }),

/***/ "./src/app/profile/profile.component.ts":
/*!**********************************************!*\
  !*** ./src/app/profile/profile.component.ts ***!
  \**********************************************/
/*! exports provided: ProfileComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "ProfileComponent", function() { return ProfileComponent; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm2015/core.js");
/* harmony import */ var _profile_service__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ./profile.service */ "./src/app/profile/profile.service.ts");
/* harmony import */ var rxjs_operators__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! rxjs/operators */ "./node_modules/rxjs/_esm2015/operators/index.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm2015/router.js");
/* harmony import */ var _angular_material__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! @angular/material */ "./node_modules/@angular/material/esm2015/material.js");
/* harmony import */ var _addnews_addnews_component__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ./addnews/addnews.component */ "./src/app/profile/addnews/addnews.component.ts");







let ProfileComponent = class ProfileComponent {
    constructor(profileService, router, dialog) {
        this.profileService = profileService;
        this.dialog = dialog;
        this.users = [];
        // users: any = [];
        this.news = [];
        this.userName = [];
        this.statusSelected = false;
        // сообщения об ошибках авторизации
        this.errors = [];
        this.router = router;
    }
    receiveUserAvatar($event) {
        this.userAvatar = $event;
        this.users[0].avatar = $event;
        console.log('users:', this.users[0]);
        this.profileService.updateUser(this.users[0]).subscribe(response => {
            console.log(response);
        }, error => {
            this.errors = error['error'];
            console.log('error', this.errors);
        });
    }
    ngOnInit() {
        this.token = localStorage.getItem('authToken');
        const token_parts = this.token.split(/\./);
        const token_decoded = JSON.parse(window.atob(token_parts[1]));
        this.usernameToken = token_decoded.username;
        this.userIDToken = token_decoded.user_id;
        this.userId = Number(this.router.url.split(':')[1]);
        //USER
        this.profileService.getUser(this.userId)
            .pipe(Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_3__["first"])()).subscribe(response => {
            // console.log(response);
            this.users.push(response);
            this.userName = this.users[0].username;
            this.userAvatar = this.users[0].avatar;
        }, error => {
            this.errors = error['error'];
            console.log('error', this.errors);
        });
        console.log('avatar:', this.userAvatar);
        //NEWS
        this.profileService.getUserNews()
            .pipe(Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_3__["first"])()).subscribe(response => {
            this.news = response['results'];
            this.news = this.news.filter(newsitem => {
                return newsitem.author === this.userId;
            });
            this.news = this.news.map(newsitem => {
                newsitem.author_id = this.userId;
                newsitem.author = this.userName;
                return newsitem;
            });
        }, error => {
            this.errors = error['error'];
            console.log('error', this.errors);
        });
    }
    onChangePage(pageOfItems) {
        // update current page of items
        this.pageOfItems = pageOfItems;
    }
    onSelect(user) {
        this.statusSelected = !this.statusSelected;
        this.selectedUser = user;
        this.selectedUser.avatar = this.userAvatar;
        console.log('this.selectedUser:', this.selectedUser);
    }
    updateUser() {
        this.profileService.updateUser(this.selectedUser).subscribe(response => {
            console.log(response);
            // this.router.navigate(['/auth/login']);
        }, error => {
            this.errors = error['error'];
            console.log('error', this.errors);
        });
    }
    openDialog() {
        const dialogRef = this.dialog.open(_addnews_addnews_component__WEBPACK_IMPORTED_MODULE_6__["AddNewsComponent"], { width: '640px', disableClose: true });
    }
};
ProfileComponent.ctorParameters = () => [
    { type: _profile_service__WEBPACK_IMPORTED_MODULE_2__["ProfileService"] },
    { type: _angular_router__WEBPACK_IMPORTED_MODULE_4__["Router"] },
    { type: _angular_material__WEBPACK_IMPORTED_MODULE_5__["MatDialog"] }
];
ProfileComponent = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
        selector: 'app-profile',
        template: __webpack_require__(/*! raw-loader!./profile.component.html */ "./node_modules/raw-loader/index.js!./src/app/profile/profile.component.html"),
        providers: [_profile_service__WEBPACK_IMPORTED_MODULE_2__["ProfileService"]],
        styles: [__webpack_require__(/*! ./profile.component.css */ "./src/app/profile/profile.component.css")]
    })
], ProfileComponent);



/***/ }),

/***/ "./src/app/profile/profile.module.ts":
/*!*******************************************!*\
  !*** ./src/app/profile/profile.module.ts ***!
  \*******************************************/
/*! exports provided: ProfileModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "ProfileModule", function() { return ProfileModule; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm2015/core.js");
/* harmony import */ var _angular_common__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/common */ "./node_modules/@angular/common/fesm2015/common.js");
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/forms */ "./node_modules/@angular/forms/fesm2015/forms.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm2015/router.js");
/* harmony import */ var _pagination_pagination_module__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ../pagination/pagination.module */ "./src/app/pagination/pagination.module.ts");
/* harmony import */ var _auth_auth_module__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ../auth/auth.module */ "./src/app/auth/auth.module.ts");
/* harmony import */ var _tags_tags_module__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! ../tags/tags.module */ "./src/app/tags/tags.module.ts");
/* harmony import */ var _profile_material_module__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(/*! ./profile-material.module */ "./src/app/profile/profile-material.module.ts");
/* harmony import */ var _news_news_module__WEBPACK_IMPORTED_MODULE_9__ = __webpack_require__(/*! ../news/news.module */ "./src/app/news/news.module.ts");
/* harmony import */ var _profile_component__WEBPACK_IMPORTED_MODULE_10__ = __webpack_require__(/*! ./profile.component */ "./src/app/profile/profile.component.ts");
/* harmony import */ var _canceladdnews_canceladdnews_module__WEBPACK_IMPORTED_MODULE_11__ = __webpack_require__(/*! ./canceladdnews/canceladdnews.module */ "./src/app/profile/canceladdnews/canceladdnews.module.ts");
/* harmony import */ var _addnews_addnews_module__WEBPACK_IMPORTED_MODULE_12__ = __webpack_require__(/*! ./addnews/addnews.module */ "./src/app/profile/addnews/addnews.module.ts");
/* harmony import */ var _fileupload_fileupload_module__WEBPACK_IMPORTED_MODULE_13__ = __webpack_require__(/*! ../fileupload/fileupload.module */ "./src/app/fileupload/fileupload.module.ts");














let ProfileModule = class ProfileModule {
};
ProfileModule = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
        imports: [
            _angular_common__WEBPACK_IMPORTED_MODULE_2__["CommonModule"],
            _profile_material_module__WEBPACK_IMPORTED_MODULE_8__["ProfileMaterialModule"],
            _angular_forms__WEBPACK_IMPORTED_MODULE_3__["FormsModule"],
            _angular_router__WEBPACK_IMPORTED_MODULE_4__["RouterModule"],
            _pagination_pagination_module__WEBPACK_IMPORTED_MODULE_5__["PaginationModule"],
            _auth_auth_module__WEBPACK_IMPORTED_MODULE_6__["AuthModule"],
            _tags_tags_module__WEBPACK_IMPORTED_MODULE_7__["TagsModule"],
            _canceladdnews_canceladdnews_module__WEBPACK_IMPORTED_MODULE_11__["CancelAddNewsModule"],
            _addnews_addnews_module__WEBPACK_IMPORTED_MODULE_12__["AddNewsModule"],
            _news_news_module__WEBPACK_IMPORTED_MODULE_9__["NewsModule"],
            _fileupload_fileupload_module__WEBPACK_IMPORTED_MODULE_13__["FileUploadModule"]
        ],
        declarations: [_profile_component__WEBPACK_IMPORTED_MODULE_10__["ProfileComponent"]],
        exports: [_profile_material_module__WEBPACK_IMPORTED_MODULE_8__["ProfileMaterialModule"]],
        entryComponents: [_profile_component__WEBPACK_IMPORTED_MODULE_10__["ProfileComponent"]],
        providers: []
    })
], ProfileModule);



/***/ }),

/***/ "./src/app/profile/profile.service.ts":
/*!********************************************!*\
  !*** ./src/app/profile/profile.service.ts ***!
  \********************************************/
/*! exports provided: ProfileService */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "ProfileService", function() { return ProfileService; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm2015/core.js");
/* harmony import */ var _angular_common_http__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/common/http */ "./node_modules/@angular/common/fesm2015/http.js");



let ProfileService = class ProfileService {
    constructor(http) {
        this.http = http;
        this.baseurl = 'http://localhost:8002';
    }
    getUser(userId) {
        return this.http.get(this.baseurl + '/users/' + userId + '/', {
            headers: {
                Authorization: localStorage.getItem('authToken'),
                'Content-Type': 'application/json'
            }
        });
    }
    getUserNews() {
        return this.http.get(this.baseurl + '/news/', {
            headers: {
                Authorization: localStorage.getItem('authToken'),
                'Content-Type': 'application/json'
            }
        });
    }
    updateUser(userData) {
        console.log(userData);
        return this.http.put(this.baseurl + '/users/' + userData.id + '/', userData, {
            headers: {
                Authorization: localStorage.getItem('authToken'),
                'Content-Type': 'application/json'
            }
        });
    }
    addNews(newsData) {
        console.log('newsData', newsData);
        return this.http.post(this.baseurl + '/news/', newsData);
    }
};
ProfileService.ctorParameters = () => [
    { type: _angular_common_http__WEBPACK_IMPORTED_MODULE_2__["HttpClient"] }
];
ProfileService = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Injectable"])({
        providedIn: 'root'
    })
], ProfileService);



/***/ }),

/***/ "./src/app/tags/tags-material.module.ts":
/*!**********************************************!*\
  !*** ./src/app/tags/tags-material.module.ts ***!
  \**********************************************/
/*! exports provided: TagsMaterialModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "TagsMaterialModule", function() { return TagsMaterialModule; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm2015/core.js");
/* harmony import */ var _angular_material__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/material */ "./node_modules/@angular/material/esm2015/material.js");



let TagsMaterialModule = class TagsMaterialModule {
};
TagsMaterialModule = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
        declarations: [],
        imports: [
            _angular_material__WEBPACK_IMPORTED_MODULE_2__["MatButtonModule"],
            _angular_material__WEBPACK_IMPORTED_MODULE_2__["MatChipsModule"],
            _angular_material__WEBPACK_IMPORTED_MODULE_2__["MatCardModule"],
            _angular_material__WEBPACK_IMPORTED_MODULE_2__["MatIconModule"],
            _angular_material__WEBPACK_IMPORTED_MODULE_2__["MatInputModule"],
            _angular_material__WEBPACK_IMPORTED_MODULE_2__["MatToolbarModule"],
            _angular_material__WEBPACK_IMPORTED_MODULE_2__["MatSelectModule"],
            _angular_material__WEBPACK_IMPORTED_MODULE_2__["MatAutocompleteModule"],
            _angular_material__WEBPACK_IMPORTED_MODULE_2__["MatInputModule"],
            _angular_material__WEBPACK_IMPORTED_MODULE_2__["MatCheckboxModule"],
            _angular_material__WEBPACK_IMPORTED_MODULE_2__["MatDialogModule"],
            _angular_material__WEBPACK_IMPORTED_MODULE_2__["MatFormFieldModule"],
        ],
        exports: [
            _angular_material__WEBPACK_IMPORTED_MODULE_2__["MatButtonModule"],
            _angular_material__WEBPACK_IMPORTED_MODULE_2__["MatChipsModule"],
            _angular_material__WEBPACK_IMPORTED_MODULE_2__["MatCardModule"],
            _angular_material__WEBPACK_IMPORTED_MODULE_2__["MatIconModule"],
            _angular_material__WEBPACK_IMPORTED_MODULE_2__["MatInputModule"],
            _angular_material__WEBPACK_IMPORTED_MODULE_2__["MatToolbarModule"],
            _angular_material__WEBPACK_IMPORTED_MODULE_2__["MatSelectModule"],
            _angular_material__WEBPACK_IMPORTED_MODULE_2__["MatAutocompleteModule"],
            _angular_material__WEBPACK_IMPORTED_MODULE_2__["MatInputModule"],
            _angular_material__WEBPACK_IMPORTED_MODULE_2__["MatCheckboxModule"],
            _angular_material__WEBPACK_IMPORTED_MODULE_2__["MatDialogModule"],
            _angular_material__WEBPACK_IMPORTED_MODULE_2__["MatFormFieldModule"],
        ],
    })
], TagsMaterialModule);



/***/ }),

/***/ "./src/app/tags/tags.component.css":
/*!*****************************************!*\
  !*** ./src/app/tags/tags.component.css ***!
  \*****************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IiIsImZpbGUiOiJzcmMvYXBwL3RhZ3MvdGFncy5jb21wb25lbnQuY3NzIn0= */"

/***/ }),

/***/ "./src/app/tags/tags.component.ts":
/*!****************************************!*\
  !*** ./src/app/tags/tags.component.ts ***!
  \****************************************/
/*! exports provided: TagsComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "TagsComponent", function() { return TagsComponent; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_cdk_keycodes__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/cdk/keycodes */ "./node_modules/@angular/cdk/esm2015/keycodes.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm2015/core.js");
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/forms */ "./node_modules/@angular/forms/fesm2015/forms.js");
/* harmony import */ var rxjs_operators__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! rxjs/operators */ "./node_modules/rxjs/_esm2015/operators/index.js");





/**
 * @title Chips Autocomplete
 */
let TagsComponent = class TagsComponent {
    constructor() {
        this.visible = true;
        this.selectable = true;
        this.removable = true;
        this.addOnBlur = true;
        this.separatorKeysCodes = [_angular_cdk_keycodes__WEBPACK_IMPORTED_MODULE_1__["ENTER"], _angular_cdk_keycodes__WEBPACK_IMPORTED_MODULE_1__["COMMA"]];
        this.tagCtrl = new _angular_forms__WEBPACK_IMPORTED_MODULE_3__["FormControl"]();
        // public tags: string[] = [];
        // allTags: string[] = [];
        this.tags = [];
        this.allTags = ['Hot', 'Crime', 'Weather', 'Music', 'Cinema'];
        this.tagsEvent = new _angular_core__WEBPACK_IMPORTED_MODULE_2__["EventEmitter"]();
        this.filteredTags = this.tagCtrl.valueChanges.pipe(Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_4__["startWith"])(null), Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_4__["map"])((tag) => tag ? this._filter(tag) : this.allTags.slice()));
    }
    ngOnInit() {
    }
    sendTags() {
        this.tagsEvent.emit(this.tags);
    }
    add(event) {
        // Add tag only when MatAutocomplete is not open
        // To make sure this does not conflict with OptionSelected Event
        if (!this.matAutocomplete.isOpen) {
            const input = event.input;
            const value = event.value;
            // Add our tag
            if ((value || '').trim()) {
                this.tags.push(value.trim());
                console.log('tags', this.tags);
            }
            // Reset the input value
            if (input) {
                input.value = '';
            }
            this.tagCtrl.setValue(null);
            this.sendTags();
        }
    }
    remove(tag) {
        const index = this.tags.indexOf(tag);
        if (index >= 0) {
            this.tags.splice(index, 1);
            console.log('tags', this.tags);
        }
    }
    selected(event) {
        this.tags.push(event.option.viewValue);
        console.log('tags', this.tags);
        this.tagInput.nativeElement.value = '';
        this.tagCtrl.setValue(null);
    }
    _filter(value) {
        const filterValue = value.toLowerCase();
        return this.allTags.filter(tag => tag.toLowerCase().indexOf(filterValue) === 0);
    }
};
tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_2__["ViewChild"])('tagInput', { static: false })
], TagsComponent.prototype, "tagInput", void 0);
tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_2__["ViewChild"])('auto', { static: false })
], TagsComponent.prototype, "matAutocomplete", void 0);
tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_2__["Output"])()
], TagsComponent.prototype, "tagsEvent", void 0);
TagsComponent = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_2__["Component"])({
        selector: 'app-tags',
        template: __webpack_require__(/*! raw-loader!./tags.component.html */ "./node_modules/raw-loader/index.js!./src/app/tags/tags.component.html"),
        styles: [__webpack_require__(/*! ./tags.component.css */ "./src/app/tags/tags.component.css")]
    })
], TagsComponent);



/***/ }),

/***/ "./src/app/tags/tags.module.ts":
/*!*************************************!*\
  !*** ./src/app/tags/tags.module.ts ***!
  \*************************************/
/*! exports provided: TagsModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "TagsModule", function() { return TagsModule; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm2015/core.js");
/* harmony import */ var _tags_component__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ./tags.component */ "./src/app/tags/tags.component.ts");
/* harmony import */ var _angular_common__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/common */ "./node_modules/@angular/common/fesm2015/common.js");
/* harmony import */ var _tags_material_module__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ./tags-material.module */ "./src/app/tags/tags-material.module.ts");
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! @angular/forms */ "./node_modules/@angular/forms/fesm2015/forms.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm2015/router.js");







let TagsModule = class TagsModule {
};
TagsModule = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
        imports: [_angular_common__WEBPACK_IMPORTED_MODULE_3__["CommonModule"], _tags_material_module__WEBPACK_IMPORTED_MODULE_4__["TagsMaterialModule"], _angular_forms__WEBPACK_IMPORTED_MODULE_5__["FormsModule"], _angular_router__WEBPACK_IMPORTED_MODULE_6__["RouterModule"], _angular_forms__WEBPACK_IMPORTED_MODULE_5__["ReactiveFormsModule"]],
        declarations: [_tags_component__WEBPACK_IMPORTED_MODULE_2__["TagsComponent"]],
        exports: [_tags_component__WEBPACK_IMPORTED_MODULE_2__["TagsComponent"]],
        entryComponents: [_tags_component__WEBPACK_IMPORTED_MODULE_2__["TagsComponent"]]
    })
], TagsModule);



/***/ }),

/***/ "./src/app/users/users-material.module.ts":
/*!************************************************!*\
  !*** ./src/app/users/users-material.module.ts ***!
  \************************************************/
/*! exports provided: UsersMaterialModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "UsersMaterialModule", function() { return UsersMaterialModule; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm2015/core.js");
/* harmony import */ var _angular_material__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/material */ "./node_modules/@angular/material/esm2015/material.js");



let UsersMaterialModule = class UsersMaterialModule {
};
UsersMaterialModule = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
        declarations: [],
        imports: [
            _angular_material__WEBPACK_IMPORTED_MODULE_2__["MatButtonModule"],
            _angular_material__WEBPACK_IMPORTED_MODULE_2__["MatChipsModule"],
            _angular_material__WEBPACK_IMPORTED_MODULE_2__["MatCardModule"],
            _angular_material__WEBPACK_IMPORTED_MODULE_2__["MatIconModule"],
            _angular_material__WEBPACK_IMPORTED_MODULE_2__["MatInputModule"],
            _angular_material__WEBPACK_IMPORTED_MODULE_2__["MatToolbarModule"],
            _angular_material__WEBPACK_IMPORTED_MODULE_2__["MatSelectModule"],
            _angular_material__WEBPACK_IMPORTED_MODULE_2__["MatAutocompleteModule"],
            _angular_material__WEBPACK_IMPORTED_MODULE_2__["MatInputModule"],
            _angular_material__WEBPACK_IMPORTED_MODULE_2__["MatCheckboxModule"],
            _angular_material__WEBPACK_IMPORTED_MODULE_2__["MatDialogModule"],
            _angular_material__WEBPACK_IMPORTED_MODULE_2__["MatFormFieldModule"],
        ],
        exports: [
            _angular_material__WEBPACK_IMPORTED_MODULE_2__["MatButtonModule"],
            _angular_material__WEBPACK_IMPORTED_MODULE_2__["MatChipsModule"],
            _angular_material__WEBPACK_IMPORTED_MODULE_2__["MatCardModule"],
            _angular_material__WEBPACK_IMPORTED_MODULE_2__["MatIconModule"],
            _angular_material__WEBPACK_IMPORTED_MODULE_2__["MatInputModule"],
            _angular_material__WEBPACK_IMPORTED_MODULE_2__["MatToolbarModule"],
            _angular_material__WEBPACK_IMPORTED_MODULE_2__["MatSelectModule"],
            _angular_material__WEBPACK_IMPORTED_MODULE_2__["MatAutocompleteModule"],
            _angular_material__WEBPACK_IMPORTED_MODULE_2__["MatInputModule"],
            _angular_material__WEBPACK_IMPORTED_MODULE_2__["MatCheckboxModule"],
            _angular_material__WEBPACK_IMPORTED_MODULE_2__["MatDialogModule"],
            _angular_material__WEBPACK_IMPORTED_MODULE_2__["MatFormFieldModule"],
        ],
    })
], UsersMaterialModule);



/***/ }),

/***/ "./src/app/users/users.component.css":
/*!*******************************************!*\
  !*** ./src/app/users/users.component.css ***!
  \*******************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "/* usersComponent's private CSS styles */\n.users {\n    margin: 0 0 2em 0;\n    list-style-type: none;\n    padding: 0;\n    width: 15em;\n  }\n.users li {\n    cursor: pointer;\n    position: relative;\n    left: 0;\n    background-color: #EEE;\n    margin: .5em;\n    padding: .3em 0;\n    height: 2.6em;\n    border-radius: 4px;\n  }\n.users li:hover {\n    color: #607D8B;\n    background-color: #DDD;\n    left: .1em;\n  }\n.users li.selected {\n    background-color: #3f51b5;\n    color: white;\n  }\n.users li.selected:hover {\n    background-color: #4757b3d4;\n    color: white;\n  }\n.users .badge {\n    display: inline-block;\n    font-size: small;\n    color: white;\n    padding: 0.8em 0.7em 0 0.7em;\n    background-color:#405061;\n    line-height: 1em;\n    position: relative;\n    left: -1px;\n    top: -4px;\n    height: 2.8em;\n    margin-right: .8em;\n    border-radius: 4px 0 0 4px;\n  }\na:focus, a:hover {\n    color: #ffffff;\n    text-decoration: none;\n  }\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbInNyYy9hcHAvdXNlcnMvdXNlcnMuY29tcG9uZW50LmNzcyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiQUFBQSx3Q0FBd0M7QUFDeEM7SUFDSSxpQkFBaUI7SUFDakIscUJBQXFCO0lBQ3JCLFVBQVU7SUFDVixXQUFXO0VBQ2I7QUFDQTtJQUNFLGVBQWU7SUFDZixrQkFBa0I7SUFDbEIsT0FBTztJQUNQLHNCQUFzQjtJQUN0QixZQUFZO0lBQ1osZUFBZTtJQUNmLGFBQWE7SUFDYixrQkFBa0I7RUFDcEI7QUFDQTtJQUNFLGNBQWM7SUFDZCxzQkFBc0I7SUFDdEIsVUFBVTtFQUNaO0FBQ0E7SUFDRSx5QkFBeUI7SUFDekIsWUFBWTtFQUNkO0FBQ0E7SUFDRSwyQkFBMkI7SUFDM0IsWUFBWTtFQUNkO0FBQ0E7SUFDRSxxQkFBcUI7SUFDckIsZ0JBQWdCO0lBQ2hCLFlBQVk7SUFDWiw0QkFBNEI7SUFDNUIsd0JBQXdCO0lBQ3hCLGdCQUFnQjtJQUNoQixrQkFBa0I7SUFDbEIsVUFBVTtJQUNWLFNBQVM7SUFDVCxhQUFhO0lBQ2Isa0JBQWtCO0lBQ2xCLDBCQUEwQjtFQUM1QjtBQUNBO0lBQ0UsY0FBYztJQUNkLHFCQUFxQjtFQUN2QiIsImZpbGUiOiJzcmMvYXBwL3VzZXJzL3VzZXJzLmNvbXBvbmVudC5jc3MiLCJzb3VyY2VzQ29udGVudCI6WyIvKiB1c2Vyc0NvbXBvbmVudCdzIHByaXZhdGUgQ1NTIHN0eWxlcyAqL1xuLnVzZXJzIHtcbiAgICBtYXJnaW46IDAgMCAyZW0gMDtcbiAgICBsaXN0LXN0eWxlLXR5cGU6IG5vbmU7XG4gICAgcGFkZGluZzogMDtcbiAgICB3aWR0aDogMTVlbTtcbiAgfVxuICAudXNlcnMgbGkge1xuICAgIGN1cnNvcjogcG9pbnRlcjtcbiAgICBwb3NpdGlvbjogcmVsYXRpdmU7XG4gICAgbGVmdDogMDtcbiAgICBiYWNrZ3JvdW5kLWNvbG9yOiAjRUVFO1xuICAgIG1hcmdpbjogLjVlbTtcbiAgICBwYWRkaW5nOiAuM2VtIDA7XG4gICAgaGVpZ2h0OiAyLjZlbTtcbiAgICBib3JkZXItcmFkaXVzOiA0cHg7XG4gIH1cbiAgLnVzZXJzIGxpOmhvdmVyIHtcbiAgICBjb2xvcjogIzYwN0Q4QjtcbiAgICBiYWNrZ3JvdW5kLWNvbG9yOiAjREREO1xuICAgIGxlZnQ6IC4xZW07XG4gIH1cbiAgLnVzZXJzIGxpLnNlbGVjdGVkIHtcbiAgICBiYWNrZ3JvdW5kLWNvbG9yOiAjM2Y1MWI1O1xuICAgIGNvbG9yOiB3aGl0ZTtcbiAgfVxuICAudXNlcnMgbGkuc2VsZWN0ZWQ6aG92ZXIge1xuICAgIGJhY2tncm91bmQtY29sb3I6ICM0NzU3YjNkNDtcbiAgICBjb2xvcjogd2hpdGU7XG4gIH1cbiAgLnVzZXJzIC5iYWRnZSB7XG4gICAgZGlzcGxheTogaW5saW5lLWJsb2NrO1xuICAgIGZvbnQtc2l6ZTogc21hbGw7XG4gICAgY29sb3I6IHdoaXRlO1xuICAgIHBhZGRpbmc6IDAuOGVtIDAuN2VtIDAgMC43ZW07XG4gICAgYmFja2dyb3VuZC1jb2xvcjojNDA1MDYxO1xuICAgIGxpbmUtaGVpZ2h0OiAxZW07XG4gICAgcG9zaXRpb246IHJlbGF0aXZlO1xuICAgIGxlZnQ6IC0xcHg7XG4gICAgdG9wOiAtNHB4O1xuICAgIGhlaWdodDogMi44ZW07XG4gICAgbWFyZ2luLXJpZ2h0OiAuOGVtO1xuICAgIGJvcmRlci1yYWRpdXM6IDRweCAwIDAgNHB4O1xuICB9XG4gIGE6Zm9jdXMsIGE6aG92ZXIge1xuICAgIGNvbG9yOiAjZmZmZmZmO1xuICAgIHRleHQtZGVjb3JhdGlvbjogbm9uZTtcbiAgfSJdfQ== */"

/***/ }),

/***/ "./src/app/users/users.component.ts":
/*!******************************************!*\
  !*** ./src/app/users/users.component.ts ***!
  \******************************************/
/*! exports provided: UsersComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "UsersComponent", function() { return UsersComponent; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm2015/core.js");
/* harmony import */ var _users_service__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ./users.service */ "./src/app/users/users.service.ts");
/* harmony import */ var rxjs_operators__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! rxjs/operators */ "./node_modules/rxjs/_esm2015/operators/index.js");




let UsersComponent = class UsersComponent {
    constructor(usersService) {
        this.usersService = usersService;
        this.users = [];
    }
    ngOnInit() {
        this.usersService.getAllUsers()
            .pipe(Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_3__["first"])()).subscribe(response => {
            this.users = response['results'];
        });
    }
    onChangePage(pageOfItems) {
        // update current page of items
        this.pageOfItems = pageOfItems;
    }
    onSelect(user) {
        this.selectedUser = user;
    }
};
UsersComponent.ctorParameters = () => [
    { type: _users_service__WEBPACK_IMPORTED_MODULE_2__["UsersService"] }
];
UsersComponent = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
        selector: 'app-users',
        template: __webpack_require__(/*! raw-loader!./users.component.html */ "./node_modules/raw-loader/index.js!./src/app/users/users.component.html"),
        providers: [_users_service__WEBPACK_IMPORTED_MODULE_2__["UsersService"]],
        styles: [__webpack_require__(/*! ./users.component.css */ "./src/app/users/users.component.css")]
    })
], UsersComponent);



/***/ }),

/***/ "./src/app/users/users.module.ts":
/*!***************************************!*\
  !*** ./src/app/users/users.module.ts ***!
  \***************************************/
/*! exports provided: UsersModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "UsersModule", function() { return UsersModule; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm2015/core.js");
/* harmony import */ var _users_component__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ./users.component */ "./src/app/users/users.component.ts");
/* harmony import */ var _angular_common__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/common */ "./node_modules/@angular/common/fesm2015/common.js");
/* harmony import */ var _users_material_module__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ./users-material.module */ "./src/app/users/users-material.module.ts");
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! @angular/forms */ "./node_modules/@angular/forms/fesm2015/forms.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm2015/router.js");
/* harmony import */ var _pagination_pagination_module__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! ../pagination/pagination.module */ "./src/app/pagination/pagination.module.ts");
/* harmony import */ var _auth_auth_module__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(/*! ../auth/auth.module */ "./src/app/auth/auth.module.ts");









let UsersModule = class UsersModule {
};
UsersModule = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
        imports: [_angular_common__WEBPACK_IMPORTED_MODULE_3__["CommonModule"],
            _users_material_module__WEBPACK_IMPORTED_MODULE_4__["UsersMaterialModule"],
            _angular_forms__WEBPACK_IMPORTED_MODULE_5__["FormsModule"],
            _angular_router__WEBPACK_IMPORTED_MODULE_6__["RouterModule"],
            _pagination_pagination_module__WEBPACK_IMPORTED_MODULE_7__["PaginationModule"],
            _auth_auth_module__WEBPACK_IMPORTED_MODULE_8__["AuthModule"]
        ],
        declarations: [_users_component__WEBPACK_IMPORTED_MODULE_2__["UsersComponent"]],
        entryComponents: [_users_component__WEBPACK_IMPORTED_MODULE_2__["UsersComponent"]]
    })
], UsersModule);



/***/ }),

/***/ "./src/app/users/users.service.ts":
/*!****************************************!*\
  !*** ./src/app/users/users.service.ts ***!
  \****************************************/
/*! exports provided: UsersService */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "UsersService", function() { return UsersService; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm2015/core.js");
/* harmony import */ var _angular_common_http__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/common/http */ "./node_modules/@angular/common/fesm2015/http.js");



let UsersService = class UsersService {
    constructor(http) {
        this.http = http;
        this.baseurl = 'http://localhost:8002';
        this.httpHeaders = new _angular_common_http__WEBPACK_IMPORTED_MODULE_2__["HttpHeaders"]({ 'Content-Type': 'application/json' });
    }
    getAllUsers() {
        return this.http.get(this.baseurl + '/users/');
    }
};
UsersService.ctorParameters = () => [
    { type: _angular_common_http__WEBPACK_IMPORTED_MODULE_2__["HttpClient"] }
];
UsersService = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Injectable"])({
        providedIn: 'root'
    })
], UsersService);



/***/ }),

/***/ "./src/environments/environment.ts":
/*!*****************************************!*\
  !*** ./src/environments/environment.ts ***!
  \*****************************************/
/*! exports provided: environment */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "environment", function() { return environment; });
// This file can be replaced during build by using the `fileReplacements` array.
// `ng build --prod` replaces `environment.ts` with `environment.prod.ts`.
// The list of file replacements can be found in `angular.json`.
const environment = {
    production: false
};
/*
 * For easier debugging in development mode, you can import the following file
 * to ignore zone related error stack frames such as `zone.run`, `zoneDelegate.invokeTask`.
 *
 * This import should be commented out in production mode because it will have a negative impact
 * on performance if an error is thrown.
 */
// import 'zone.js/dist/zone-error';  // Included with Angular CLI.


/***/ }),

/***/ "./src/main.ts":
/*!*********************!*\
  !*** ./src/main.ts ***!
  \*********************/
/*! no exports provided */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm2015/core.js");
/* harmony import */ var _angular_platform_browser_dynamic__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/platform-browser-dynamic */ "./node_modules/@angular/platform-browser-dynamic/fesm2015/platform-browser-dynamic.js");
/* harmony import */ var _app_app_module__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ./app/app.module */ "./src/app/app.module.ts");
/* harmony import */ var _environments_environment__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ./environments/environment */ "./src/environments/environment.ts");




if (_environments_environment__WEBPACK_IMPORTED_MODULE_3__["environment"].production) {
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["enableProdMode"])();
}
Object(_angular_platform_browser_dynamic__WEBPACK_IMPORTED_MODULE_1__["platformBrowserDynamic"])().bootstrapModule(_app_app_module__WEBPACK_IMPORTED_MODULE_2__["AppModule"])
    .catch(err => console.error(err));


/***/ }),

/***/ 0:
/*!***************************!*\
  !*** multi ./src/main.ts ***!
  \***************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

module.exports = __webpack_require__(/*! /home/user/denisenko_angular_client/src/main.ts */"./src/main.ts");


/***/ })

},[[0,"runtime","vendor"]]]);
//# sourceMappingURL=main-es2015.js.map