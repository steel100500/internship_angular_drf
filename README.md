### All requests must be localhost or your local ip_address Ex.:192.168.2.15(for internal network for other computers)
### you can find and replace it if it needs by global finder in your IDE

## HOW TO BUILD THE PROJECT

# --------------------------- Backend ---------------------------

## First of all, you should prepare database PostgreSql for the project data
https://www.digitalocean.com/community/tutorials/how-to-use-postgresql-with-your-django-application-on-ubuntu-14-04

```bash
sudo su - postgres

psql

CREATE DATABASE news;

CREATE USER newsuser WITH PASSWORD 'newspassword';

ALTER ROLE newsuser SET client_encoding TO 'utf8';

ALTER ROLE newsuser SET default_transaction_isolation TO 'read committed';

ALTER ROLE newsuser SET timezone TO 'UTC';

GRANT ALL PRIVILEGES ON DATABASE news TO newsuser;
```

## Now you should deploy DjangoRestFramework
```bash
cd django_rest/news

pip install virtualenv

virtualenv myprojectenv

source myprojectenv/bin/activate

python -V

pip install -r requirements.txt

python manage.py makemigrations

python manage.py migrate

python manage.py runserver 0.0.0.0:8002
```

# --------------------------- Frontend ---------------------------

```bash
cd CLIENT

npm install

ng serve --disable-host-check
```

### Congratilations!
## Let's go to the web browser and check it:
## Backend: http://localhost:8002/
## Frontend: http://localhost:4200

### If you have problems with runing backend 
### you should check version of python in active enviroment 
### it should be >= python3.5
### how to create enviroment with concrect version 
### of python there are several ways. Just google it.

## For GOOGLE authorization you should run the project like open website in the global network

### ngrok helps in this case

https://dashboard.ngrok.com/get-started

RUN ANGULAR:
```bash
ng serve --disable-host-check
```
RUN ngrok

```bash
./ngrok http 4200
```
You should create api-google key and add this url

which was created by ngrok to the allowed Google urls settings.

See this manual: https://maxpfrontend.ru/vebinary/avtorizatsiya-s-pomoschyu-google-sign-in/

After that put the google key to the

CLIENT/src/app/app.module.ts on the line GoogleLoginProvider
